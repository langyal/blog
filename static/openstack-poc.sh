#! /bin/bash

set -x

PATH=/snap/bin:$PATH

qemu-img convert -f qcow2 -O raw cirros.img cirros.raw

. openrc
openstack catalog list
openstack flavor create --ram 1024 --disk 8 --vcpus 1 m1.small
openstack image create --public --container-format=bare --disk-format=raw --file cirros.raw cirros
openstack keypair create --public-key ~/.ssh/authorized_keys mykey
openstack network create --external --provider-physical-network physnet1 --provider-network-type flat net-external
openstack subnet create --network net-external --subnet-range 10.44.0.0/16 --allocation-pool start=10.44.1.1,end=10.44.255.254 --gateway 10.44.0.1 --no-dhcp net-external_subnet
openstack router create --project admin --project-domain admin_domain provider-router
openstack router set --external-gateway net-external provider-router
openstack router add subnet provider-router net-external_subnet
openstack network create net-internal
openstack subnet create --network net-internal --subnet-range 10.55.0.0/16 --dns-nameserver=10.235.119.37 --dns-nameserver=10.235.119.38 net-internal_subnet
openstack router add subnet provider-router net-internal_subnet
for i in `openstack security group list -f value -c ID`; do
 openstack security group rule create $i --protocol icmp --remote-ip 0.0.0.0/0
 openstack security group rule create $i --protocol tcp --remote-ip 0.0.0.0/0 --dst-port 22
done
openstack server create --flavor m1.small --image cirros --key-name mykey --network net-internal vm0
openstack server create --flavor m1.small --image cirros --key-name mykey --network net-internal vm1
openstack subnet set --no-dns-nameservers net-external_subnet
openstack subnet set --dns-nameserver=10.235.119.37 --dns-nameserver=10.235.119.38 net-external_subnet
openstack subnet set --dhcp net-external_subnet
openstack server create --flavor m1.small --image cirros --key-name mykey --network net-external vm2
FIP=`openstack floating ip create net-external -f value -c floating_ip_address`
openstack server add floating ip vm0 $FIP
openstack volume create --size 4 vo0
openstack volume create --size 4 vo1
openstack volume create --size 4 vo2
openstack server add volume vm0 vo0
openstack server add volume vm1 vo1
openstack server add volume vm2 vo2

