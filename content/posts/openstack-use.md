+++
title = "Use your own Openstack"
date = 2018-11-18T16:19:29+01:00
draft = false
tags = []
categories = []
+++

`GOAL` of this post: **show how to use your openstack deployment**

We have an ubuntu/juju/maas based openstack deployment - let's use it! We have to

- get access to the deployment
  - gather credentials
  - access the cloud via the dashboard (cli usage will be covered in a later post)
- prepare the deployment
  - create a project/tenant and a user, assign the user to the project
  - upload an image
  - create a provider subnet/network
  - create a tenant subnet/network
  - create a tenant router
  - allocate a floating ip
- use the deployment
  - launch an instance and assign a floating ip to it
  - access the instance

### get access to the environment
You probably noticed that we haven't provided any ip address/passwords during the deployment; everything were generated for us. So now we have to grab it:

find the ip address of the dashboard:
```
u@hyp:~$ juju status --format=yaml openstack-dashboard | grep public-address | awk '{print $2}'
192.168.100.12
```

open this URL with your favourite browser:

- `http://192.168.100.12/horizon`
- username: `admin`
- password: oops, this was not specified either, so get it this way:

find the keystone unit:
```
u@hyp:~$ juju status keystone --format yaml | awk '/units:$/ {getline; gsub(/:$/, ""); print $1}'
keystone/0
```

get the password:
```
u@hyp:~$ juju run --unit keystone/0 'leader-get admin_passwd'
seag8aehoom1Aiku
```

after logging in, you should see something like this:
![dashboard](/60lab-dashboard.png)

before we continue, make sure you have these **Subnets** in MaaS:
![maas-subnets](/60lab-maas-subnets.png)

we should now **reserve** a range in MaaS:
![maas-reserve](/60lab-maas-reserve.png)

ok, let's continue with the OpenStack configuration

### create two projects: **proj0** and **proj1**
![os-projects](/60lab-os-projects.png)

### create two users and assign them to proj0 and proj1, respectively: **user0** and **user1**
![os-user0](/60lab-os-user0.png)
![os-user1](/60lab-os-user1.png)

### provider network
```
- create provider network `provider0`
  - Admin / System / Networks
  - project: admin
  - provider network type: local
  - shared, external
```
![os-provider-network](/60lab-os-provider-network.png)

once created, click on **provider0** and:
```
- create subnet `subnet0`: 192.168.200.0/24
  - use just a range: 192.168.200.191,192.168.200.250
  - gw: 192.168.200.2
  - ns: 192.168.200.2
```
![os-provider-subnet](/60lab-os-provider-subnet.png)
![os-provider-subnet-details](/60lab-os-provider-subnet-details.png)

note: we use our MaaS VM as default gateway and nameserver

### upload cloud images
we will use two cloud images: **cirros** and **ubuntu**
```
- download the cloud images to your local computer
- create images under System/Images
```
![os-provider-image-cirros](/60lab-os-provider-image-cirros.png)
![os-provider-image-ubuntu](/60lab-os-provider-image-ubuntu.png)

you should see this:
![os-provider-images](/60lab-os-provider-images.png)

### create a new flavor
we will need it later
![os-provider-myflavor](/60lab-os-provider-myflavor.png)

### tenant configuration
log out from the dashboard as admin and log in as **user0**

create a network:
```
- create tenant network: `localnet0`
  - subnet: 10.10.10.0/24
  - gw: 10.10.10.1
  - ns: 192.168.200.2
```
![os-proj0-network](/60lab-os-proj0-network.png)

you should have these networks now:
![os-proj0-networks](/60lab-os-proj0-networks.png)

the next step is to create a tenant router:
```
- create tenant router: `router0`
  - interfaces: `provider0` + `localnet0`
  - external ip: 192.168.200.194
  - attached 2nd interface to localnet0, ip: 10.10.10.1
  - this is the gw now in localnet0, provides SNAT by default
```
![os-proj0-createrouter](/60lab-os-proj0-createrouter.png)
![os-proj0-router](/60lab-os-proj0-router.png)

add in _internal_ interface to this router:
![os-proj0-router-addif](/60lab-os-proj0-router-addif.png)

### access & security
we are very close... but first we have to

modify the default security rules: add **ssh** and **icmp** to the allowed protocols:
![os-proj0-secrules](/60lab-os-proj0-secrules.png)

add your ssh keypair:
![os-proj0-keypair](/60lab-os-proj0-keypair.png)

allocate a floating ip:
![os-proj0-fip](/60lab-os-proj0-fip.png)


### launch the first instance
showtime!
![os-proj0-inst0-detail](/60lab-os-proj0-inst0-detail.png)
![os-proj0-inst0-source](/60lab-os-proj0-inst0-source.png)
![os-proj0-inst0-flavor](/60lab-os-proj0-inst0-flavor.png)
![os-proj0-inst0-network](/60lab-os-proj0-inst0-network.png)

once it's running, associate a floting ip to it:
![os-proj0-inst0-fip](/60lab-os-proj0-inst0-fip.png)

we are done:
![os-proj0-inst0-launched](/60lab-os-proj0-inst0-launched.png)

log in from the hypervisor and check connectivity:
```
u@hyp:~$ ssh cirros@192.168.200.193
$ ping gmail.com
PING gmail.com (172.217.20.5): 56 data bytes
64 bytes from 172.217.20.5: seq=0 ttl=50 time=6.758 ms
64 bytes from 172.217.20.5: seq=1 ttl=50 time=6.927 ms
64 bytes from 172.217.20.5: seq=2 ttl=50 time=6.176 ms
^C
--- gmail.com ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max = 6.176/6.620/6.927 ms
```

### launch the second instance
- switch to user1/proj1
- repeat the steps above
  - create a network/subnet
  - create a router
  - modify the default security rules
  - add your ssh keypair
  - allocate a floating ip
- just to see what **multi tenancy** means
  - use the same ip ranges what you used as user0/proj0
  - name it **instance0** as you did before
- launch an ubuntu instance, use **myflavor** this time because
  - m1.tiny is too small for ubuntu
  - m1.small is too big for our mini-hypervisors (don't forget that we simulate them with VMs having just 3GB RAM)
- once it's running, associate a floting ip to it
![os-proj1-inst0-launched](/60lab-os-proj1-inst0-launched.png)

log in from the hypervisor:
```
u@hyp:~$ ssh ubuntu@192.168.200.195
```

this is not enough - let's launch a container inside this instance:
```
ubuntu@instance0:~$ sudo lxd init
Do you want to configure a new storage pool (yes/no) [default=yes]?
Name of the storage backend to use (dir or zfs) [default=dir]:
Would you like LXD to be available over the network (yes/no) [default=no]?
Do you want to configure the LXD bridge (yes/no) [default=yes]?
Would you like to setup a network bridge for LXD containers now? [yes/no] yes
Bridge interface name: lxdbr0
Do you want to setup an IPv4 subnet? [yes/no] yes
IPv4 address: 10.0.8.1
IPv4 CIDR mask: 24
First DHCP address: 10.0.8.2
Last DHCP address: 10.0.8.11
Max number of DHCP clients: 10
Do you want to NAT the IPv4 traffic? [yes/no] yes
Do you want to setup an IPv6 subnet? [yes/no] no

ubuntu@instance0:~$ sudo lxc remote add --protocol simplestreams ubuntu-minimal https://cloud-images.ubuntu.com/minimal/releases/

ubuntu@instance0:~$ sudo lxc launch ubuntu-minimal:16.04 lx0
```

log in to the container, install ping and check connectivity:
```
ubuntu@instance0:~$ sudo lxc exec lx0 bash
root@lx0:~# apt install -y iputils-ping
root@lx0:~# ping ubuntu.com
PING ubuntu.com (91.189.94.40) 56(84) bytes of data.
64 bytes from ovinnik.canonical.com (91.189.94.40): icmp_seq=1 ttl=44 time=40.8 ms
64 bytes from ovinnik.canonical.com (91.189.94.40): icmp_seq=2 ttl=44 time=37.2 ms
64 bytes from ovinnik.canonical.com (91.189.94.40): icmp_seq=3 ttl=44 time=37.8 ms
```

### summary
let me recall now this picture:
![0001-2lab-network](/0001-2lab-network.png)

the life of a packet can be followed this way:

- **source**: the _lx0_ lxd container; the first snat "happens" on instance0: 10.0.8.0/24 -> 10.10.10.5
- next hop: the openstack instance _instance0_, which is a nested-kvm
- next hop: the nova compute node - whichever was selected by the scheduler -, which is a kvm
- next hop: the neutron router, snat: 10.10.10.0/24 -> 192.168.200.194,  which is an ip namespace on the neutron-gateway, which is a kvm
- next hop: the maas vm, snat:192.168.200.0/24 -> 192.168.122.199, which is a kvm
- next hop: the kvm hypervisor, snat: 192.168.122.0/24 -> 192.168.1.8, which is a baremetal (my notebook)
- next hop: my-home-router, snat: 192.168.1.0/24 -> 84.236.79.56, which is a baremetal
- **destination**: who knows what's on the other side of the internet...

### references
- https://docs.openstack.org/project-deploy-guide/charm-deployment-guide/latest/install-openstack.html

