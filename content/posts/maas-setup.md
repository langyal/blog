+++
title = "Metal as a Service"
date = 2018-09-24T15:04:51+02:00
draft = false
tags = []
categories = []
+++

`GOAL` of this post: **build your own MAAS server**

MaaS or MAAS or maas = "Metal as a Service" is a great opensource tool from Canonical. It is depicted nicely here: https://maas.io/how-it-works

We will create a MaaS VM with the help of `uvtool` which is a really cloudy way of launching a VM: you can use cloud images.

### install uvtool, get an image
```
u@hyp:~$ sudo apt -y install uvtool
u@hyp:~$ uvt-simplestreams-libvirt sync release=xenial arch=amd64
u@hyp:~$ uvt-simplestreams-libvirt query
release=xenial arch=amd64 label=release (20180806)
```

now let's prepare a bit our installation:

### add two more interfaces
- modify the default template
- create a postconfig script

```
@hyp:~$ cp /usr/share/uvtool/libvirt/template.xml .
@hyp:~$ diff /usr/share/uvtool/libvirt/template.xml template.xml
15a16,23
>     <interface type='network'>
>       <source network='net-ext'/>
>       <model type='virtio'/>
>     </interface>
>     <interface type='network'>
>       <source network='net-int'/>
>       <model type='virtio'/>
>     </interface>

@hyp:~$ cat script-interfaces.sh
#! /bin/sh

IF=ens4
cat <<EOT >/etc/network/interfaces.d/$IF.cfg
auto $IF
iface $IF inet static
    address 192.168.100.2
    netmask 255.255.255.0
EOT
ifup $IF

IF=ens5
cat <<EOT >/etc/network/interfaces.d/$IF.cfg
auto $IF
iface $IF inet static
    address 192.168.200.2
    netmask 255.255.255.0
EOT
ifup $IF
```

### launch and start using the instance

```
@hyp:~$ uvt-kvm create maas release=xenial --memory 2048 --cpu 1 --disk 20 --template template.xml --run-script-once scr
ipt-interfaces.sh

...wait...

@hyp:~$ uvt-kvm ip maas
192.168.122.119

@hyp:~$ uvt-kvm ssh maas
ubuntu@maas:~$ ip a | grep "inet.*ens"
    inet 192.168.122.119/24 brd 192.168.122.255 scope global ens3
    inet 192.168.100.2/24 brd 192.168.100.255 scope global ens4
    inet 192.168.200.2/24 brd 192.168.200.255 scope global ens5
```

## add libvirt packages
- with this step we will be able to switch on/off our VMs from MaaS

```
- update as usual
ubuntu@maas:~$ sudo apt update
ubuntu@maas:~$ sudo apt upgrade -y

- add libvirt packages -> we can start/stop VMs via maas
ubuntu@maas:~$ sudo apt install -y qemu-kvm libvirt-bin

- let the user `ubuntu` to run virsh
ubuntu@maas:~$ sudo usermod -aG libvirtd ubuntu
```

### turn maas VM into a NAT router
- this is a bit tricky: why do we want this? well, this way we _simplify_ and _control_ the way our additional VMs can reach the internet

```
- enable ip_forwarding
ubuntu@maas:~$ sudo bash -c "echo 1 > /proc/sys/net/ipv4/ip_forward"

- to make it permanent:
ubuntu@maas:~$ grep net.ipv4.ip_forward= /etc/sysctl.conf
net.ipv4.ip_forward=1

- configure SNAT
ubuntu@maas:~$ sudo iptables -t nat -A POSTROUTING -s 192.168.100.0/24 -o ens3 -j MASQUERADE
ubuntu@maas:~$ sudo iptables -t nat -A POSTROUTING -s 192.168.200.0/24 -o ens3 -j MASQUERADE

- to make it permanent:
ubuntu@maas:~$ sudo apt install -y iptables-persistent # say "yes" to save current rules to /etc/iptables/rules.v4
```

### maas initial configuration
```
- add maas packages
ubuntu@maas:~$ sudo apt install -y maas
ubuntu@maas:~$ sudo maas createadmin
Username: maasadmin
Password: ******
Again: ******
Email: maasadmin@lab.local
Import SSH keys [] (lp:user-id or gh:user-id):

- access to login page
[client]$ ssh u@hyp -L5240:192.168.100.2:5240
url: http://localhost:5240/MAAS/
username: maasadmin
password: ******

```

### maas configuration
login to the MaaS GUI and apply the following settings:

```
- maas region: `lab.local`

- maas images:
  - 18.04 LTS
  - 16.04 LTS

- upload your ssh public key

- settings / Global Kernel Parameters
   net.ifnames=0

- settings / Proxy / Don't use a proxy
  - why we remove proxy? because
    - maas would provide it's first ip as proxy ip, which is from the default libvirt network: 192.168.122.0/24
    - this would be a problem later: deploying the nova-compute component installs libvirt with the same default network
    - setting the proxy manually to the _good_ interface is a nice idea, but maas _stops_ the running proxy in this case -> it will not work, either
    - the really nice solution would be to reconfigure the default libvirt network on the hypervisor

- enable dhcp
  - on the fabric where our net-ext (192.168.100.0/24) resides
  - click on "VLAN" "untagged" and choose action "Provide DHCP"
  - change the default Gateway IP from 192.168.100.1 to 192.168.100.2
    - this is our maas server
    - that's why we converted it into a SNAT GW in a previous step
  - set nameserver to 192.168.100.2 also, otherwise
    - maas would provide it's first ip, which is from the default libvirt network: 192.168.122.0/24
    - this would be a problem later: deploying the nova-compute component installs libvirt with the same default network
    - so name resolution would not work just because of routing (homework: think it over)
  - on the fabric where our net-int (192.168.200.0/24) resides
    - remove default gw, we don't need it

```

### launch & commission a VM

```
  - use virt-manager
  - create a new VM with these parameters:
    - 1 CPU
    - 4GB RAM
    - 1 NIC on network `net-ext`
    - 1 NIC on network `net-int`
  - make sure it is booting over the network `192.168.100.0/24`
  - maas will answer the boot request and list the node as "new"
  - change the node's name to `node-a` in maas
  - configure the power type of the "new" node:
    - qemu+ssh://ubuntu@192.168.122.1/system
    - password:
    - virsh vm id: node-a
  - comission the node
    - remove validation from hardware tests (this is a VM :)
    - wait until comissioning finishes
  - deploy ubuntu
    - configure the 2nd interface also
| Name | PXE Type | Fabric   | VLAN     | Subnet           | IP Address |
| ---- | -------- | -------- | -------- | ---------------- | ---------- |
| ens3 | Physical | fabric-1 | untagged | 192.168.100.0/24 | 192.168.100.3 (Auto assign) |
| ens9 | Physical | fabric-2 | untagged | 192.168.200.0/24 | 192.168.200.3 (Auto assign) |
```

### summary
You have a MaaS VM deployed & configured. You have successfully commissioned and deployed an Ubuntu VM via MaaS. Congratulations.


### references:
- https://help.ubuntu.com/lts/serverguide/cloud-images-and-uvtool.html
- https://www.cyberciti.biz/faq/how-to-use-kvm-cloud-images-on-ubuntu-linux/
- https://docs.openstack.org/project-deploy-guide/charm-deployment-guide/latest/install-maas.html


