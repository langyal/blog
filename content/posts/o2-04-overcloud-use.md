
## use the over-overcloud

### basics
```
ubuntu@r0:~$ cd ~/lab/openstack/openstack-base-bionic-train
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ . openrc
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack catalog list
+-----------+--------------+-----------------------------------------------------------------------+
| Name      | Type         | Endpoints                                                             |
+-----------+--------------+-----------------------------------------------------------------------+
| keystone  | identity     | RegionOne                                                             |
|           |              |   public: http://10.33.2.9:5000/v3                                    |
|           |              | RegionOne                                                             |
|           |              |   admin: http://10.33.2.9:35357/v3                                    |
|           |              | RegionOne                                                             |
|           |              |   internal: http://10.33.2.9:5000/v3                                  |
...

ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ mkdir ~/images && cd ~/images
ubuntu@r0:~/images$ wget http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ cd -
```

### flavor, image, keypair
```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack flavor create --ram 1024 --disk 8 --vcpus 1 m1.small
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack image create --public --container-format=bare --disk-format=raw --file ~/images/cirros-0.4.0-x86_64-disk.img cirros
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack keypair create --public-key ~/.ssh/authorized_keys mykey
```

### create an external network/subnet
```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack network create --external --provider-physical-network physnet1 --provider-network-type flat net-external
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack subnet create --network net-external --subnet-range 10.44.0.0/16 --allocation-pool start=10.44.1.1,end=10.44.255.254 --gateway 10.44.0.1 --no-dhcp net-external_subnet
```

### create a router + attach it to the net-external_subnet
```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack router create --project admin --project-domain admin_domain provider-router
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack router set --external-gateway net-external provider-router
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack router add subnet provider-router net-external_subnet
```

### create an internal network + attach the router to it
```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack network create net-internal
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack subnet create --network net-internal --subnet-range 10.55.0.0/16 --dns-nameserver=10.22.0.3 net-internal_subnet
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack router add subnet provider-router net-internal_subnet
```

### allow icmp/ssh
```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ for i in `openstack security group list -f value -c ID`; do
 openstack security group rule create $i --protocol icmp --remote-ip 0.0.0.0/0
 openstack security group rule create $i --protocol tcp --remote-ip 0.0.0.0/0 --dst-port 22
done
```

### launch and test an instance
```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack server create --flavor m1.small --image cirros --key-name mykey --network net-internal vm0
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack floating ip create net-external
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ FIP=`openstack floating ip list -f value -c 'Floating IP Address' | head -1`
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack server add floating ip vm0 $FIP
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack console log show vm0
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ ping $FIP
```

### ssh
- it will be pretty slow
- check the SG rules
- give it a try:

```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ ssh cirros@$FIP
```

### console of the vm-in-vm-in-vm
- find the "hypervisor" and the "instance":

```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ openstack server show vm0 | egrep "hyp|inst"
| OS-EXT-SRV-ATTR:hypervisor_hostname | oc-1501.maas                                             |
| OS-EXT-SRV-ATTR:instance_name       | instance-00000001                                        |

ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ juju status | grep oc.1501
3        started  10.33.2.3   oc-1501              bionic  default  Deployed
```

- ssh to the "hypervisor", open console

```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ juju ssh 3 sudo -i

root@oc-1501:~# virsh console instance-00000001
Connected to domain instance-00000001
Escape character is ^]

login as 'cirros' user. default password: 'gocubsgo'. use 'sudo' for root.
vm0 login: cirros
Password:
```

- we are in!

```
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1458 qdisc pfifo_fast qlen 1000
    link/ether fa:16:3e:76:98:bb brd ff:ff:ff:ff:ff:ff
    inet 10.55.1.77/16 brd 10.55.255.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::f816:3eff:fe76:98bb/64 scope link
       valid_lft forever preferred_lft forever

$ ip route list
default via 10.55.0.1 dev eth0
10.55.0.0/16 dev eth0  src 10.55.1.77
169.254.169.254 via 10.55.0.1 dev eth0

$ export http_proxy=10.22.0.3:8888
$ curl ifconfig.me; echo ""
62.152.138.31
```

- the life of a packet

```
cirros [ip:10.55.1.77] [vm-in-vm-in-vm]
|
---> neutron-gw [snat-to:10.44.4.64] [vm-in-vm]
     |
     ---> r0-gw [snat-to:10.22.0.3] [vm]
          |
          ---> r0-proxy[10.22.0.3:8888] [vm]
               |
--o2---------------------------------------o2-
               |
               ---> [your-underlay-openstack]
```
