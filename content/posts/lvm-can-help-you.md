---
title: "LVM Can Help You"
date: 2018-09-01T21:28:46+02:00
draft: false
---

`GOAL` of this post: **use LVM to save your ass**

This blog will be short but useful. Let's say you start playing with your hypervisor and at some point you feel you want to go back in time because something went really wrong.
Of course, it's too late... but if you used LVM snapshot, you _could_ go back in time.

So, before you start something risky, create a snapshot; e.g. before installing libvirt (which actually not really risky):

```
u@hyp:~$ sudo -i
root@hyp:~# lvcreate -L100G -s -n sn-before-libvirt /dev/vg-root/lv-root
root@hyp:~# lvs
  LV                VG      Attr       LSize   Pool Origin  Data%  Meta%  Move Log Cpy%Sync Convert
  lv-root           vg-root owi-aos--- 100,00g
  lv-swap           vg-root -wi-ao----   1,00g
  sn-before-libvirt vg-root swi-a-s--- 100,00g      lv-root 0,01
```

...now do whatever you want...

### if you want to keep the changes, get rid of the snapshot:

```
root@hyp:~# lvremove /dev/vg-root/sn-before-libvirt
Do you really want to remove and DISCARD active logical volume vg-root/sn-before-libvirt? [y/n]: y
  Logical volume "sn-before-libvirt" successfully removed
root@hyp:~# lvs
  LV      VG      Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv-root vg-root -wi-ao---- 100,00g
  lv-swap vg-root -wi-ao----   1,00g
```

### if you want _go back in time_ and drop the changes, do this:

```
root@hyp:~# lvconvert --merge /dev/vg-root/sn-before-libvirt
  Can't merge until origin volume is closed.
  Merging of snapshot vg-root/sn-before-libvirt will occur on next activation of vg-root/lv-root.
root@hyp:~# reboot
root@hyp:~# lvs
  LV      VG      Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv-root vg-root -wi-ao---- 100,00g
  lv-swap vg-root -wi-ao----   1,00g
```

You can find it a bit strange at first look that by _removing_ the snapshot you _keep the changes_ and by _merging_ it you _drop the changes_. In fact, when you create a snapshot, you say something like this: "follow this block; if it changes, make a copy (copy-on-write) of it". So a snapshot will contain the _changes_ since the creation of the snapshot. Thus, if you want to keep the _current state_ you simply discard the snapshot (lvremove) - and if you want to have the _previous state_, you have to merge the snapshot.

### add more space to your volume
let's say you reserved some physical space on your disk (as I suggested) and you run out of space in your logical volume/filesystem; add 100GB now:

```
- we need 100GB more
root@hyp:~# lvextend -L +100G /dev/vg-root/lv-root
  Size of logical volume vg-root/lv-root changed from 200,00 GiB (51200 extents) to 300,00 GiB (76800 extents).
  Logical volume vg-root/lv-root successfully resized.

- grow filesystem
root@hyp:~# resize2fs /dev/mapper/vg--root-lv--root
resize2fs 1.44.1 (24-Mar-2018)
Filesystem at /dev/mapper/vg--root-lv--root is mounted on /; on-line resizing required
old_desc_blocks = 25, new_desc_blocks = 38
The filesystem on /dev/mapper/vg--root-lv--root is now 78643200 (4k) blocks long.
```

