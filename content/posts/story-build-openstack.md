+++
title = "Story: run your own Openstack"
date = 2018-08-27T20:43:01+02:00
draft = false
tags = []
categories = []
+++

Now let's jump into the middle, the first story in Columbo-style:

`TARGET`: **run your own Openstack deployment at home**

The outcome is this: `ping ubuntu.com` Wow. Is it really so exciting? Usually not; but in our case, we will initiate those icmp packets

```
> from an lxd container, which is running in a
  > VM, which is running as a kvm/qemu (nested virtualization) VM on a
    > nova compute node, which is running as a kvm/qemu VM on a
      > hypervisor, which is a real hardware (baremetal)
```

I know people like pictures, so this is the same, depicted nicely :) - focus on *lx0* in the middle:

![overview](/0001-0lab-overview.png)

The same, now from the network point of view; our friend *lx0* is labeled as *container* here:

![network](/0001-2lab-network.png)

Finally, this is the Openstack design:

![network](/0001-1lab-design.png)

This is ambitious. Let's break it down into separate posts:

* build your own LAB
* set up and use MAAS
* set up and use JUJU
* set up and use OPENSTACK

