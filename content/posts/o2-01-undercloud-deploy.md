
## undercloud
- note: if there is no prompt, the command must be executed from an authorized client

### public key
```
openstack keypair create --public-key ~/.ssh/authorized_keys langyal
```

### network: net-undercloud
```
NET=net-undercloud
SUB=10.22.0.0/16
DNS=8.8.8.8

openstack network create $NET
openstack subnet create --network $NET --subnet-range $SUB --dns-nameserver=$DNS ${NET}_subnet
```

### allow ssh
```
SRC=`curl -s ifconfig.me`/32

openstack security group rule create default --protocol tcp --remote-ip $SRC --dst-port 22
```

### instance: r0
```
FLV=sn1.small
IMG=ubuntu-18.04-x86_64
KEY=langyal
NET=net-undercloud
INS=r0

openstack server create --flavor $FLV --image $IMG --key-name $KEY --network $NET $INS
```

### attach FIP
```
INS=r0
FIP=`openstack floating ip list -f value -c 'Floating IP Address' | head -1`

openstack server add floating ip $INS $FIP
```

### prepare r0
- login
```
ssh ubuntu@$FIP
```

- update packages
```
ubuntu@r0:~$ sudo apt update && sudo apt upgrade -y
```

- install tinyproxy
```
ubuntu@r0:~$ sudo apt install tinyproxy -y
ubuntu@r0:~$ sudo vi /etc/tinyproxy/tinyproxy.conf
ubuntu@r0:~$ grep ^Allow /etc/tinyproxy/tinyproxy.conf
Allow 127.0.0.1
Allow 10.0.0.0/8
ubuntu@r0:~$ sudo systemctl restart tinyproxy
```

- install bind (brrr) - dnscache is not maintened any more :(
```
ubuntu@r0:~$ sudo apt install bind9 bind9utils bind9-doc bind9-host
ubuntu@r0:~$ sudo systemctl start bind9
ubuntu@r0:~$ sudo systemctl enable bind9
ubuntu@r0:~$ sudo vi /etc/bind/named.conf.options
...
        // hide version number from clients for security reasons.
        version "not currently available";

        // optional - BIND default behavior is recursion
        recursion yes;

        // provide recursion service to trusted clients only
        allow-recursion { 127.0.0.1; 10.0.0.0/8; };

        // enable the query log
        querylog yes;
...
ubuntu@r0:~$ sudo systemctl restart bind9
ubuntu@r0:~$ sudo vi /etc/systemd/resolved.conf
...
[Resolve]
DNS=127.0.0.1
...
ubuntu@r0:~$ sudo systemctl restart systemd-resolved
```

- adjust net-undercloud nameserver
```
NET=net-undercloud
DNS=10.22.0.3
openstack subnet set --dns-nameserver=$DNS ${NET}_subnet
NET=net-undercloud
DNS=8.8.8.8
openstack subnet unset --dns-nameserver=$DNS ${NET}_subnet
```

- install openntpd
```
ubuntu@r0:~$ sudo apt install openntpd -y
```

- install ansible
```
ubuntu@r0:~$ sudo apt install ansible -y
```

- install virsh
```
ubuntu@r0:~$ sudo apt install libvirt-bin -y
ubuntu@r0:~$ virsh net-destroy default  # optional
ubuntu@r0:~$ virsh net-undefine default # optional
```

- install maas cli
```
ubuntu@r0:~$ sudo apt install maas-cli -y
```

- install jq
```
ubuntu@r0:~$ sudo apt install jq -y
```

- install unzip
```
ubuntu@r0:~$ sudo apt install unzip -y
```
- install openstack client
```
ubuntu@r0:~$ sudo apt install python3-openstackclient -y
```

### instances: uc-node11..18
```
FLV=sn1.large
IMG=ubuntu-16.04-x86_64
KEY=langyal
NET=net-undercloud
INS=uc-node

for i in 11 12 13 14 15 16 17 18; do openstack server create --flavor $FLV --image $IMG --key-name $KEY --network $NET ${INS}${i}; done
```

### volumes
```
INS=uc-node
VOL=uc-vol
SIZ=100

for i in 11 12 13 14 15 16 17 18; do openstack volume create --size $SIZ ${VOL}${i}; done
for i in 11 12 13 14 15 16 17 18; do openstack server add volume ${INS}${i} ${VOL}${i}; done

```

### now we can use ansible
- create an inventory from openstack for the op.sys (/etc/hosts) and for ansible

```
openstack server list -f value -c Name -c Networks >x

cat x | sed -e 's/net-undercloud=//g' -e 's/,.*$//g' | awk '{printf "%s\t%s\n",$2,$1}' | sort -k 2,2 >etc-hosts
cat >>etc-hosts <<EOF
10.33.0.11	oc-11maas
10.33.0.12	oc-12maas
10.33.0.13	oc-13maas
EOF

>ansible-hosts
cat >>ansible-hosts <<EOF
[uc:vars]
ansible_python_interpreter=/usr/bin/python3
[oc:vars]
ansible_python_interpreter=/usr/bin/python3
EOF
echo "[uc]" >>ansible-hosts
grep uc- etc-hosts | awk '{print $2}' >>ansible-hosts
echo "[oc]" >>ansible-hosts
grep oc- etc-hosts | awk '{print $2}' >>ansible-hosts
```

- ansible ping
  - homework: copy `etc-hosts` to `r0:/etc/hosts`
  - homework: copy `ansible-hosts` to `r0:/etc/ansible/hosts`

```
ubuntu@r0:~$ grep ^10 /etc/hosts
10.22.0.3	r0
10.22.0.4	uc-node11
10.22.0.5	uc-node12
10.22.0.6	uc-node13
10.22.0.7	uc-node14
10.22.0.8	uc-node15
10.22.0.9	uc-node16
10.22.0.10	uc-node17
10.22.0.11	uc-node18
10.33.0.11      oc-11maas
10.33.0.12      oc-12maas
10.33.0.13      oc-13maas
```

```
ubuntu@r0:~$ cat /etc/ansible/hosts
[uc:vars]
ansible_python_interpreter=/usr/bin/python3
[oc:vars]
ansible_python_interpreter=/usr/bin/python3
[uc]
uc-node11
uc-node12
uc-node13
uc-node14
uc-node15
uc-node16
uc-node17
uc-node18
[oc]
oc-11maas
oc-12maas
oc-13maas
```

```
ubuntu@r0:~$ ansible -m ping uc
```

- run the prepared playbook - you can find what you need [here](/r0.tgz)
```
ubuntu@r0:~/lab/ansible$ ansible-playbook plays/uc-hypervisor.yml
```

- create a dedicated pool for vm-in-vm images

```
ubuntu@r0:~/lab/ansible$ ansible uc -a "virsh pool-define-as img dir - - - - /img"
ubuntu@r0:~/lab/ansible$ ansible uc -a "virsh pool-start img"
ubuntu@r0:~/lab/ansible$ ansible uc -a "virsh pool-autostart img"
```

