+++
title = "Openstack Deployment"
date = 2018-11-06T08:20:41+01:00
draft = false
tags = []
categories = []
+++

`GOAL` of this post: **deploying an openstack with maas/juju**

It's showtime: we will deploy a working private cloud now, step-by-step.
This is usually _not_ the way you do it in production: you'd use a **bundle** - and even better a pipeline that includes the bundle. I will cover that in a different post.

So, let's start with some design:

- we will use a simple network approach now: all "underlay" traffic will go over `net-ext`, except
  - ceph cluster traffic, it will use `net-int`
  - tenant external network access: it will use `net-int` as well (not for security reasons :)
- we need 4 VMs commissioned in maas
- 3 of them (node-a,b,c) will be tagged as _converged_ and will host `compute + ceph + swift + openstack control`
- 1 of them (node-n) will be tagged as _network_ and will host just `neutron-gateway`
- _baremetal_ below refers to a VM in our lab
- _container_ below refers to an lxd container running in a VM

| unit      | tag       | on baremetal | in container |
|:---------:|:---------:|--------------|--------------|
| node-a    | converged | nova-compute, ceph-osd, swift-object | ceph-mon, neutron-api, nova-cloud-controller, openstack-dashboard |
| node-b    | converged | nova-compute, ceph-osd, swift-object | mysql, glance, cinder, ceph-radosgw |
| node-c    | converged | nova-compute, ceph-osd, swift-object | rabbitmq-server, keystone, swift-proxy |
| node-n    | network   | neutron-gateway | - |

![network](/0001-1lab-design.png)

We go step-by-step, so we will

- create a juju model
- deploy just ubuntu on the nodes, verify
  - connectivity
  - disks
- add openstack components one-by-one
- _relate_ juju units, which is the final step before you can use your cloud

### add a juju model called "uos" = ubuntu open stack
```
juju add-model uos lab.local
```

### deploy the first ubuntu baremetal (VM)
```
juju deploy --constraints tags=converged ubuntu
```
wait until it finishes - check juju status regularly

### check networking and disks
```
u@hyp:~$ juju ssh ubuntu/0 sudo "fdisk -l|grep \"Disk /dev/sd\""
Disk /dev/sda: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk /dev/sdb: 8 GiB, 8589934592 bytes, 16777216 sectors
Disk /dev/sdc: 8 GiB, 8589934592 bytes, 16777216 sectors
Connection to 192.168.200.59 closed.

u@hyp:~$ juju ssh ubuntu/0 sudo "ip a; ip ro ls"
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 52:54:00:08:ea:45 brd ff:ff:ff:ff:ff:ff
    inet 192.168.100.25/24 brd 192.168.100.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::5054:ff:fe08:ea45/64 scope link
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 52:54:00:49:d6:91 brd ff:ff:ff:ff:ff:ff
    inet 192.168.200.59/24 brd 192.168.200.255 scope global eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::5054:ff:fe49:d691/64 scope link
       valid_lft forever preferred_lft forever
default via 192.168.100.2 dev eth0 onlink
192.168.100.0/24 dev eth0  proto kernel  scope link  src 192.168.100.25
192.168.200.0/24 dev eth1  proto kernel  scope link  src 192.168.200.59
Connection to 192.168.200.59 closed.
```

### deploy two more ubuntu baremetals (VMs)
```
juju add-unit -n 2 ubuntu
```
note: no need to specify "--constraints tags=converged": it is implicitly applied

### start building the control plane: mysql, rabbitmq, keystone
```
juju deploy --bind external --to lxd:1 percona-cluster mysql
juju deploy --bind external --to lxd:2 rabbitmq-server
juju deploy --bind external --to lxd:2 keystone
```
this step takes some time since the first containers can be created only after downloading the container images into the baremetals (VMs)

check the status at any time:
```
u@hyp:~$ juju status
Model  Controller  Cloud/Region  Version  SLA          Timestamp
uos    jc-maas     lab.local     2.4.1    unsupported  11:29:16+01:00

App              Version  Status       Scale  Charm            Store       Rev  OS      Notes
keystone                  maintenance      1  keystone         jujucharms  286  ubuntu
mysql                     maintenance      1  percona-cluster  jujucharms  270  ubuntu
rabbitmq-server           maintenance      1  rabbitmq-server  jujucharms   80  ubuntu
ubuntu           16.04    active           3  ubuntu           jujucharms   12  ubuntu

Unit                Workload     Agent      Machine  Public address  Ports  Message
keystone/0*         maintenance  executing  2/lxd/1  192.168.100.22         (install) installing charm software
mysql/0*            maintenance  executing  1/lxd/0  192.168.100.21         (install) installing charm software
rabbitmq-server/0*  maintenance  executing  2/lxd/0  192.168.100.37         (install) installing charm software
ubuntu/0            active       idle       0        192.168.100.25         ready
ubuntu/1            active       idle       1        192.168.100.24         ready
ubuntu/2*           active       idle       2        192.168.100.19         ready

Machine  State    DNS             Inst id              Series  AZ       Message
0        started  192.168.100.25  fcqy7f               xenial  default  Deployed
1        started  192.168.100.24  qm6kf7               xenial  default  Deployed
1/lxd/0  started  192.168.100.21  juju-cfcba5-1-lxd-0  xenial  default  Container started
2        started  192.168.100.19  thayya               xenial  default  Deployed
2/lxd/0  started  192.168.100.37  juju-cfcba5-2-lxd-0  xenial  default  Container started
2/lxd/1  started  192.168.100.22  juju-cfcba5-2-lxd-1  xenial  default  Container started
```

### check nested virtualization first:
```
u@hyp:~$ juju run --application ubuntu sudo kvm-ok
- Stdout: |
    INFO: /dev/kvm exists
    KVM acceleration can be used
  UnitId: ubuntu/0
- Stdout: |
    INFO: /dev/kvm exists
    KVM acceleration can be used
  UnitId: ubuntu/1
- Stdout: |
    INFO: /dev/kvm exists
    KVM acceleration can be used
  UnitId: ubuntu/2
```
note: don't worry if you have no such feature - see the next step

### deploy nova-compute
```
u@hyp:~$ juju deploy --to 0 --config nova-compute.yaml nova-compute
```
you may notice that we refer to a file called "nova-compute.yaml"; you can pass parameters to the charm this way
```
u@hyp:~/lab/m-stack$ cat nova-compute.yaml
nova-compute:
  enable-live-migration: True
  enable-resize: True
  migration-auth-type: ssh
  virt-type: qemu
```
you can see that we overwrote the default "kvm" with "qemu"; if nested virtualization works for you, you can use _kvm_ as virt-type

note: changing _virt-type_ after deployment is not supported

add to more units:
```
u@hyp:~$ juju add-unit --to 1 nova-compute
u@hyp:~$ juju add-unit --to 2 nova-compute
```
now a colored version of juju status:
![juju-status](/5lab-juju-status.png)
this is OK despite of some red/blocked status: we just haven't added the missing components yet

### remove libvirt default networking
this step is not strictly required, however I have added here because it points out that openstack (nova/neutron) does not use it
```
u@hyp:~$ juju run --application nova-compute "virsh net-destroy default; virsh net-undefine default; virsh net-list"
```

go on with different openstack components:

### compute: nova-cloud-controller
```
u@hyp:~$ juju deploy --bind external --to lxd:0 --config nova-controller.yaml nova-cloud-controller

u@hyp:~$ cat nova-controller.yaml
nova-cloud-controller:
  network-manager: "Neutron"
```

### network: neutron-gateway, openvswitch
```
u@hyp:~$ juju deploy --constraints tags=network --config neutron.yaml neutron-gateway
u@hyp:~$ juju deploy --bind external --to lxd:0 --config neutron.yaml neutron-api
u@hyp:~$ juju deploy neutron-openvswitch

u@hyp:~$ cat neutron.yaml
neutron-gateway:
  ext-port: 'eth1'
  instance-mtu: 1400
neutron-api:
  neutron-security-groups: True
```
note: now we have four nodes, we run the neutron-gateway on a dedicated baremetal (VM), tagged with "network"

### storage: glance, cinder
```
u@hyp:~$ juju deploy --bind external --to lxd:1 glance
u@hyp:~$ juju deploy --bind external --to lxd:1 --config cinder.yaml cinder

u@hyp:~$ cat cinder.yaml
cinder:
  glance-api-version: 2
  block-device: None
```

we continue with ceph; you will see how we define a different network for the ceph cluster traffic

## storage: ceph-osd, ceph-mon
```
u@hyp:~$ juju deploy --bind "public=external cluster=internal" --to 0 --config ceph-osd.yaml ceph-osd
u@hyp:~$ juju add-unit --to 1 ceph-osd
u@hyp:~$ juju add-unit --to 2 ceph-osd

u@hyp:~$ cat ceph-osd.yaml
ceph-osd:
  osd-devices: /dev/sdb

u@hyp:~$ juju deploy --bind external --to lxd:0 ceph-mon
u@hyp:~$ juju add-unit --to lxd:1 ceph-mon
u@hyp:~$ juju add-unit --to lxd:2 ceph-mon

## storage: ceph-radosgw
u@hyp:~$ juju deploy --bind external --to lxd:1 ceph-radosgw
```

swift is not strictly necessary to add since you can get (almost) the same functionality from rados-gw - but why not to try it

### storage: swift-storage, swift-proxy
```
u@hyp:~$ juju deploy --to 0 --config swift-storage.yaml swift-storage
u@hyp:~$ juju add-unit --to 1 swift-storage
u@hyp:~$ juju add-unit --to 2 swift-storage

u@hyp:~$ cat swift-storage.yaml
swift-storage:
  block-device: sdc
  overwrite: "true"

u@hyp:~$ juju deploy --bind external --to lxd:2 --config swift-proxy.yaml swift-proxy

u@hyp:~$ cat swift-proxy.yaml
swift-proxy:
  zone-assignment: auto
  swift-hash: "ef4fe8e7-66d2-4f7d-83f5-a9dee52017a7"
  region: RegionOneSWIFT
```

finally we add ntp and the dashboard (horizon) to the environment

### misc: ntp, dashboard
```
u@hyp:~$ juju deploy ntp
u@hyp:~$ juju deploy --bind external --to lxd:0 openstack-dashboard
```

at this point we will have (in juju terminology)

- _machines_ deployed - baremetals (VMs) and containers
- _units_ in active or blocked status
- _applications_ in active, waiting or blocked status

the last step is to _relate_ "things" to each other - this is one of the most amazing feature of juju; you just issue the commands and wait - the magic will happen (the prompt is omitted this time):

### relations
```
# keystone
juju add-relation keystone mysql

# nova-compute
juju add-relation nova-compute:amqp rabbitmq-server
juju add-relation nova-compute nova-cloud-controller
juju add-relation nova-compute glance

# nova-cloud-controller
juju add-relation nova-cloud-controller mysql
juju add-relation nova-cloud-controller keystone
juju add-relation nova-cloud-controller rabbitmq-server
juju add-relation nova-cloud-controller neutron-gateway
juju add-relation nova-cloud-controller glance
juju add-relation nova-cloud-controller cinder

# neutron
juju add-relation neutron-api keystone
juju add-relation neutron-api mysql
juju add-relation neutron-api neutron-gateway
juju add-relation neutron-api neutron-openvswitch
juju add-relation neutron-api rabbitmq-server
juju add-relation neutron-api nova-cloud-controller

juju add-relation neutron-openvswitch nova-compute
juju add-relation neutron-openvswitch rabbitmq-server
juju add-relation neutron-gateway:amqp rabbitmq-server:amqp

# ceph
juju add-relation ceph-osd ceph-mon
juju add-relation ceph-radosgw ceph-mon
juju add-relation ceph-radosgw keystone

# swift
juju add-relation swift-proxy swift-storage
juju add-relation swift-proxy keystone

# glance
juju add-relation glance mysql
juju add-relation glance keystone
juju add-relation glance rabbitmq-server

# cinder
juju add-relation cinder mysql
juju add-relation cinder keystone
juju add-relation cinder rabbitmq-server
juju add-relation cinder:image-service glance:image-service
juju add-relation cinder ceph-mon

# ntp
juju add-relation neutron-gateway ntp
juju add-relation ceph-osd ntp

# dashboard
juju add-relation openstack-dashboard:identity-service keystone:identity-service
```

### review of juju status
juju works asynchronously - you have to check the status from time to time; if you were patient, you'd see something similar:
```
u@hyp:~$ juju status --relations
Model  Controller  Cloud/Region  Version  SLA          Timestamp
uos    jc-maas     lab.local     2.4.1    unsupported  13:08:20+01:00

App                    Version       Status  Scale  Charm                  Store       Rev  OS      Notes
ceph-mon               10.2.10       active      3  ceph-mon               jujucharms   29  ubuntu
ceph-osd               10.2.10       active      3  ceph-osd               jujucharms  271  ubuntu
ceph-radosgw           10.2.10       active      1  ceph-radosgw           jujucharms  260  ubuntu
cinder                 8.1.1         active      1  cinder                 jujucharms  274  ubuntu
glance                 12.0.0        active      1  glance                 jujucharms  269  ubuntu
keystone               9.3.0         active      1  keystone               jujucharms  286  ubuntu
mysql                  5.6.37-26.21  active      1  percona-cluster        jujucharms  270  ubuntu
neutron-api            8.4.0         active      1  neutron-api            jujucharms  264  ubuntu
neutron-gateway        8.4.0         active      1  neutron-gateway        jujucharms  254  ubuntu
neutron-openvswitch    8.4.0         active      3  neutron-openvswitch    jujucharms  253  ubuntu
nova-cloud-controller  13.1.4        active      1  nova-cloud-controller  jujucharms  314  ubuntu
nova-compute           13.1.4        active      3  nova-compute           jujucharms  288  ubuntu
ntp                    4.2.8p4+dfsg  active      4  ntp                    jujucharms   31  ubuntu
openstack-dashboard    9.1.2         active      1  openstack-dashboard    jujucharms  268  ubuntu
rabbitmq-server        3.5.7         active      1  rabbitmq-server        jujucharms   80  ubuntu
swift-proxy            2.7.1         active      1  swift-proxy            jujucharms   72  ubuntu
swift-storage          2.7.1         active      3  swift-storage          jujucharms  247  ubuntu
ubuntu                 16.04         active      3  ubuntu                 jujucharms   12  ubuntu

Unit                      Workload  Agent      Machine  Public address  Ports           Message
ceph-mon/0                active    idle       0/lxd/2  192.168.100.18                  Unit is ready and clustered
ceph-mon/1                active    executing  1/lxd/3  192.168.100.11                  Unit is ready and clustered
ceph-mon/2*               active    idle       2/lxd/2  192.168.100.20                  Unit is ready and clustered
ceph-osd/0                active    idle       0        192.168.100.25                  Unit is ready (1 OSD)
  ntp/0*                  active    idle                192.168.100.25  123/udp         ntp: Ready
ceph-osd/1*               active    idle       1        192.168.100.24                  Unit is ready (1 OSD)
  ntp/2                   active    idle                192.168.100.24  123/udp         ntp: Ready
ceph-osd/2                active    idle       2        192.168.100.19                  Unit is ready (1 OSD)
  ntp/3                   active    idle                192.168.100.19  123/udp         ntp: Ready
ceph-radosgw/0*           active    executing  1/lxd/4  192.168.100.33  80/tcp          Unit is ready
cinder/0*                 active    idle       1/lxd/2  192.168.100.39  8776/tcp        Unit is ready
glance/0*                 active    idle       1/lxd/1  192.168.100.38  9292/tcp        Unit is ready
keystone/0*               active    idle       2/lxd/1  192.168.100.22  5000/tcp        Unit is ready
mysql/0*                  active    idle       1/lxd/0  192.168.100.21  3306/tcp        Unit is ready
neutron-api/0*            active    idle       0/lxd/1  192.168.100.23  9696/tcp        Unit is ready
neutron-gateway/0*        active    idle       3        192.168.100.35                  Unit is ready
  ntp/1                   active    idle                192.168.100.35  123/udp         ntp: Ready
nova-cloud-controller/0*  active    idle       0/lxd/0  192.168.100.36  8774/tcp        Unit is ready
nova-compute/0*           active    idle       0        192.168.100.25                  Unit is ready
  neutron-openvswitch/0*  active    idle                192.168.100.25                  Unit is ready
nova-compute/1            active    idle       1        192.168.100.24                  Unit is ready
  neutron-openvswitch/1   active    idle                192.168.100.24                  Unit is ready
nova-compute/2            active    idle       2        192.168.100.19                  Unit is ready
  neutron-openvswitch/2   active    idle                192.168.100.19                  Unit is ready
openstack-dashboard/0*    active    idle       0/lxd/3  192.168.100.12  80/tcp,443/tcp  Unit is ready
rabbitmq-server/0*        active    idle       2/lxd/0  192.168.100.37  5672/tcp        Unit is ready
swift-proxy/0*            active    idle       2/lxd/3  192.168.100.32  8080/tcp        Unit is ready
swift-storage/0*          active    idle       0        192.168.100.25                  Unit is ready
swift-storage/1           active    idle       1        192.168.100.24                  Unit is ready
swift-storage/2           active    idle       2        192.168.100.19                  Unit is ready
ubuntu/0                  active    idle       0        192.168.100.25                  ready
ubuntu/1                  active    idle       1        192.168.100.24                  ready
ubuntu/2*                 active    idle       2        192.168.100.19                  ready

Machine  State    DNS             Inst id              Series  AZ       Message
0        started  192.168.100.25  fcqy7f               xenial  default  Deployed
0/lxd/0  started  192.168.100.36  juju-cfcba5-0-lxd-0  xenial  default  Container started
0/lxd/1  started  192.168.100.23  juju-cfcba5-0-lxd-1  xenial  default  Container started
0/lxd/2  started  192.168.100.18  juju-cfcba5-0-lxd-2  xenial  default  Container started
0/lxd/3  started  192.168.100.12  juju-cfcba5-0-lxd-3  xenial  default  Container started
1        started  192.168.100.24  qm6kf7               xenial  default  Deployed
1/lxd/0  started  192.168.100.21  juju-cfcba5-1-lxd-0  xenial  default  Container started
1/lxd/1  started  192.168.100.38  juju-cfcba5-1-lxd-1  xenial  default  Container started
1/lxd/2  started  192.168.100.39  juju-cfcba5-1-lxd-2  xenial  default  Container started
1/lxd/3  started  192.168.100.11  juju-cfcba5-1-lxd-3  xenial  default  Container started
1/lxd/4  started  192.168.100.33  juju-cfcba5-1-lxd-4  xenial  default  Container started
2        started  192.168.100.19  thayya               xenial  default  Deployed
2/lxd/0  started  192.168.100.37  juju-cfcba5-2-lxd-0  xenial  default  Container started
2/lxd/1  started  192.168.100.22  juju-cfcba5-2-lxd-1  xenial  default  Container started
2/lxd/2  started  192.168.100.20  juju-cfcba5-2-lxd-2  xenial  default  Container started
2/lxd/3  started  192.168.100.32  juju-cfcba5-2-lxd-3  xenial  default  Container started
3        started  192.168.100.35  768pwb               xenial  default  Deployed

Relation provider                        Requirer                                       Interface               Type         Message
ceph-mon:client                          cinder:ceph                                    ceph-client             regular
ceph-mon:mon                             ceph-mon:mon                                   ceph                    peer
ceph-mon:osd                             ceph-osd:mon                                   ceph-osd                regular
ceph-mon:radosgw                         ceph-radosgw:mon                               ceph-radosgw            regular
ceph-osd:juju-info                       ntp:juju-info                                  juju-info               subordinate
ceph-radosgw:cluster                     ceph-radosgw:cluster                           swift-ha                peer
cinder:cinder-volume-service             nova-cloud-controller:cinder-volume-service    cinder                  regular
cinder:cluster                           cinder:cluster                                 cinder-ha               peer
glance:cluster                           glance:cluster                                 glance-ha               peer
glance:image-service                     cinder:image-service                           glance                  regular
glance:image-service                     nova-cloud-controller:image-service            glance                  regular
glance:image-service                     nova-compute:image-service                     glance                  regular
keystone:cluster                         keystone:cluster                               keystone-ha             peer
keystone:identity-service                ceph-radosgw:identity-service                  keystone                regular
keystone:identity-service                cinder:identity-service                        keystone                regular
keystone:identity-service                glance:identity-service                        keystone                regular
keystone:identity-service                neutron-api:identity-service                   keystone                regular
keystone:identity-service                nova-cloud-controller:identity-service         keystone                regular
keystone:identity-service                openstack-dashboard:identity-service           keystone                regular
keystone:identity-service                swift-proxy:identity-service                   keystone                regular
mysql:cluster                            mysql:cluster                                  percona-cluster         peer
mysql:shared-db                          cinder:shared-db                               mysql-shared            regular
mysql:shared-db                          glance:shared-db                               mysql-shared            regular
mysql:shared-db                          keystone:shared-db                             mysql-shared            regular
mysql:shared-db                          neutron-api:shared-db                          mysql-shared            regular
mysql:shared-db                          nova-cloud-controller:shared-db                mysql-shared            regular
neutron-api:cluster                      neutron-api:cluster                            neutron-api-ha          peer
neutron-api:neutron-api                  nova-cloud-controller:neutron-api              neutron-api             regular
neutron-api:neutron-plugin-api           neutron-gateway:neutron-plugin-api             neutron-plugin-api      regular
neutron-api:neutron-plugin-api           neutron-openvswitch:neutron-plugin-api         neutron-plugin-api      regular
neutron-gateway:cluster                  neutron-gateway:cluster                        quantum-gateway-ha      peer
neutron-gateway:juju-info                ntp:juju-info                                  juju-info               subordinate
neutron-gateway:quantum-network-service  nova-cloud-controller:quantum-network-service  quantum                 regular
neutron-openvswitch:neutron-plugin       nova-compute:neutron-plugin                    neutron-plugin          subordinate
nova-cloud-controller:cluster            nova-cloud-controller:cluster                  nova-ha                 peer
nova-compute:cloud-compute               nova-cloud-controller:cloud-compute            nova-compute            regular
nova-compute:compute-peer                nova-compute:compute-peer                      nova                    peer
ntp:ntp-peers                            ntp:ntp-peers                                  ntp                     peer
openstack-dashboard:cluster              openstack-dashboard:cluster                    openstack-dashboard-ha  peer
rabbitmq-server:amqp                     cinder:amqp                                    rabbitmq                regular
rabbitmq-server:amqp                     glance:amqp                                    rabbitmq                regular
rabbitmq-server:amqp                     neutron-api:amqp                               rabbitmq                regular
rabbitmq-server:amqp                     neutron-gateway:amqp                           rabbitmq                regular
rabbitmq-server:amqp                     neutron-openvswitch:amqp                       rabbitmq                regular
rabbitmq-server:amqp                     nova-cloud-controller:amqp                     rabbitmq                regular
rabbitmq-server:amqp                     nova-compute:amqp                              rabbitmq                regular
rabbitmq-server:cluster                  rabbitmq-server:cluster                        rabbitmq-ha             peer
swift-proxy:cluster                      swift-proxy:cluster                            swift-ha                peer
swift-storage:swift-storage              swift-proxy:swift-storage                      swift                   regular
```

### summary
Using juju and maas, we can quickly deploy a working openstack environment.

Openstack is "just an application" here - though a fairly complicated one; this is exactly why we need a framework to set it up on Day1 and manage it starting from Day2.

### references
- https://docs.openstack.org/project-deploy-guide/charm-deployment-guide/latest/install-openstack.html

