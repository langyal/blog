+++
title = "Juju Controller"
date = 2018-10-23T17:00:03+02:00
draft = false
tags = []
categories = []
+++

`GOAL` of this post: **configure a juju controller with a maas backend**

We have set up our maas server so that we are able to deploy "anything" - but that's not enough: we also need a higher level engine to express and deploy our design.
We will use _juju_ which is a "modeling and configuration" tool - a kind of two-in-one solution what causes problems sometimes; but let's focus on the bright side of juju now.

We have at least three components we have to talk about:

- the juju client
- the juju backend
- the juju controller

### the juju client
let's install the client on our hypervisor; note: you can install it practically anywhere, the client stores no state

```
u@hyp:~$ sudo snap install juju --classic
```

please, don't ask what _snap_ is now... just type :) - it was simple, wasn't it?

### the juju backend
this is a bit more tricky: juju is able to use a wide range of backends, including some really exotic ones:
```
u@hyp:~$ juju clouds
Cloud        Regions  Default          Type        Description
aws               15  us-east-1        ec2         Amazon Web Services
aws-china          1  cn-north-1       ec2         Amazon China
aws-gov            1  us-gov-west-1    ec2         Amazon (USA Government)
azure             26  centralus        azure       Microsoft Azure
azure-china        2  chinaeast        azure       Microsoft Azure China
cloudsigma         5  hnl              cloudsigma  CloudSigma Cloud
google            13  us-east1         gce         Google Cloud Platform
joyent             6  eu-ams-1         joyent      Joyent Cloud
oracle             5  uscom-central-1  oracle      Oracle Cloud
rackspace          6  dfw              rackspace   Rackspace Cloud
localhost          1  localhost        lxd         LXD Container Hypervisor

Try 'list-regions <cloud>' to see available regions.
'show-cloud <cloud>' or 'regions --format yaml <cloud>' can be used to see region endpoints.
Update the known public clouds with 'update-clouds'.
'add-cloud' can add private or custom clouds / infrastructure built for the following provider types:
  - maas, manual, openstack, oracle, vsphere
```

actually, this is not enough in our case; we want to use our _maas_ "cloud" as a backend, so:
```
u@hyp:~$ juju add-cloud
Since Juju 2 is being run for the first time, downloading latest cloud information.
Fetching latest public cloud list...
Your list of public clouds is up to date, see `juju clouds`.
Cloud Types
  maas
  manual
  openstack
  oracle
  vsphere

Select cloud type: maas

Enter a name for your maas cloud: lab.local

Enter the API endpoint url: http://192.168.100.2:5240/MAAS/

Cloud "lab.local" successfully added
You may bootstrap with 'juju bootstrap lab.local'

u@hyp:~$ juju clouds | grep lab.local
lab.local          0                   maas        Metal As A Service
```

the next step is to add some credentials to juju so that it can request resources from maas later:

```
u@hyp:~$ juju add-credential lab.local
Enter credential name: maasadmin

Using auth-type "oauth1".

Enter maas-oauth:

Credentials added for cloud my-maas.
```

### the juju controller (bootstrap)
this is really the final step before we can start using our maas/juju framework: we have to spawn a unit what we will use as a controller

note: as of the writing of this post it is not possible to use a juju controller that is spawned in a _different_ backend; at first attempt I tried to spawn a juju
controller on my localhost based "LXD Container Hypervisor" and use it with the maas backend thus I could save some resources on the hypervisor - but it does not work now

```
u@hyp:~$ juju bootstrap --bootstrap-series=xenial --constraints tags=juju lab.local jc-maas
Creating Juju controller "jc-maas" on lab.local
Looking for packaged Juju agent version 2.4.1 for amd64
Launching controller instance(s) on lab.local...
ERROR failed to bootstrap model: cannot start bootstrap instance: failed to acquire node: unexpected: ServerError: 400 BAD REQUEST ({"tags": ["No such tag(s): 'juju'."]})

```

well, this was expected (at least by me :) - as a first step we have to create a new node in maas:

- launch *virt-manager*
- create a new VM, name it "juju"
- 1 vcpu, 1G memory, one nic on the network _net-ext_, 8GB scsi disk, boot from network
- commission it in maas and tag it as "juju"

now retry with lower memory
```
u@hyp:~$ juju bootstrap --bootstrap-series=xenial --constraints tags=juju --constraints mem=1G lab.local jc-maas
```
wait for the process to finish - you can follow the events on the console of the VM

### juju models
you can have many _models_ managed by a single _juju controller_; in fact, the controller itself is also a model:
```
u@hyp:~$ juju models
Controller: jc-maas

Model       Cloud/Region  Status     Machines  Cores  Access  Last connection
controller  lab.local     available         1      1  admin   just now
default*    lab.local     available         0      -  admin   1 minute ago
```

the _model_ is a kind of _workspace_ or independent "playground" - so let's add our own model:
```
u@hyp:~$ juju add-model uos
Uploading credential 'lab.local/admin/maasadmin' to controller
Added 'uos' model with credential 'maasadmin' for user 'admin'
```

switch between controllers:models
```
u@hyp:~$ juju switch jc:
jc-maas:admin/uos -> jc (controller)

u@hyp:~$ juju switch jc-maas:uos
jc (controller) -> jc-maas:admin/uos
```

login to the juju controller
```
u@hyp:~$ juju ssh -m controller 0

```

### summary
We have a working juju controller and we have defined our model (uos= [u]buntu [o]pen[s]tack) - we are really close to start deploying something!

