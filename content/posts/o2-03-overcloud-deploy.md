
## overcloud deploy

### juju vm-in-vm
- prepare volume, define and start
```
ubuntu@uc-node11:~$ virsh vol-create-as img oc-11juju.img 20G --format qcow2
ubuntu@uc-node11:~$ virsh define oc-11juju.xml
ubuntu@uc-node11:~$ virsh start oc-11juju
```

- enlist and set parameters - assumption: you are logged in to maas
```
ubuntu@r0:~/lab/maas-inventory$ UC=uc-node11; OC=oc-11juju
ubuntu@r0:~/lab/maas-inventory$ MAC=`virsh --connect qemu+ssh://$UC/system dumpxml $OC | grep "mac address" | awk -F\' '{print $2}'| head -1`
ubuntu@r0:~/lab/maas-inventory$ SYSID=`maas maasadmin nodes read mac_address=$MAC | jq '.[] | .system_id' | sed 's/"//g'`
ubuntu@r0:~/lab/maas-inventory$ POWERADDRESS="qemu+ssh://ubuntu@$UC/system"
ubuntu@r0:~/lab/maas-inventory$ maas maasadmin machine update $SYSID hostname=$OC power_type=virsh power_parameters_power_address="$POWERADDRESS" power_parameters_power_id=$OC >/dev/null
```

- label it as *juju*
```
ubuntu@r0:~/lab/maas-inventory$ TAG=juju
ubuntu@r0:~/lab/maas-inventory$ maas maasadmin tag update-nodes $TAG add=$SYSID
```

- commission
```
ubuntu@r0:~/lab/maas-inventory$ maas maasadmin machine commission $SYSID
```

### juju client
```
ubuntu@r0:~$ sudo snap install juju --classic
2019-12-07T16:24:53Z INFO Waiting for restart...
juju 2.7.0 from Canonical✓ installed
```

### add maas as a juju backend

```
ubuntu@r0:~$ juju add-cloud
Since Juju 2 is being run for the first time, downloading latest cloud information.
Fetching latest public cloud list...
This client's list of public clouds is up to date, see `juju clouds --client-only`.
Cloud Types
  lxd
  maas
  manual
  openstack
  vsphere

Select cloud type: maas

Enter a name for your maas cloud: olab

Enter the API endpoint url: http://oc-11maas:5240/MAAS/

This operation can be applied to both a copy on this client and to the one on a controller.
No current controller was detected and there are no registered controllers on this client: either bootstrap one or register one.
Do you ONLY want to add cloud "olab" to this client? (Y/n):

Cloud "olab" successfully added to your local client.
You will need to add a credential for this cloud (`juju add-credential olab`)
before you can use it to bootstrap a controller (`juju bootstrap olab`) or
to create a model (`juju add-model <your model name> olab`).
```

```
ubuntu@r0:~$ juju add-credential olab
This operation can be applied to both a copy on this client and to the one on a controller.
No current controller was detected and there are no registered controllers on this client: either bootstrap one or register one.
Do you ONLY want to add a credential to this client? (Y/n):

Enter credential name: maasadmin

Regions
  default

Select region [any region, credential is not region specific]:

Using auth-type "oauth1".

Enter maas-oauth:

Credential "maasadmin" added locally for cloud "olab".
```

### juju controller bootstrap
```
ubuntu@r0:~$ juju bootstrap --bootstrap-series=xenial --constraints tags=juju --constraints mem=2G olab oc-juju
```

### neutron-gateway will run on the "control nodes"
- prepare volume, define and start
```
ubuntu@uc-node11:~$ virsh vol-create-as img oc-11nngw.img 20G --format qcow2
ubuntu@uc-node11:~$ virsh define oc-11nngw.xml
ubuntu@uc-node11:~$ virsh start oc-11nngw
```

- enlist / commission in maas - assumption: you are logged in to maas
```
ubuntu@r0:~/lab/maas-inventory$ UC=uc-node11; OC=oc-11nngw
ubuntu@r0:~/lab/maas-inventory$ MAC=`virsh --connect qemu+ssh://$UC/system dumpxml $OC | grep "mac address" | awk -F\' '{print $2}'| head -1`
ubuntu@r0:~/lab/maas-inventory$ SYSID=`maas maasadmin nodes read mac_address=$MAC | jq '.[] | .system_id' | sed 's/"//g'`
ubuntu@r0:~/lab/maas-inventory$ POWERADDRESS="qemu+ssh://ubuntu@$UC/system"
ubuntu@r0:~/lab/maas-inventory$ maas maasadmin machine update $SYSID hostname=$OC power_type=virsh power_parameters_power_address="$POWERADDRESS" power_parameters_power_id=$OC >/dev/null
```

- label it as *neutron-gateway*
```
ubuntu@r0:~$ TAG=neutron-gateway
ubuntu@r0:~$ maas maasadmin tags create name=$TAG
ubuntu@r0:~$ maas maasadmin tag update-nodes $TAG add=$SYSID
```

- commission
```
ubuntu@r0:~$ maas maasadmin machine commission $SYSID
```

### deploy a cloud into a separate model
- get and unpack he bundle
```
ubuntu@r0:~$ mkdir ~/lab/openstack && cd ~/lab/openstack
ubuntu@r0:~/lab/openstack$ wget https://api.jujucharms.com/charmstore/v5/bundle/openstack-base-65/archive
ubuntu@r0:~/lab/openstack$ mkdir openstack-base-bionic-train && cd openstack-base-bionic-train && unzip ../archive && rm ../archive
```

- modify it a bit:
  - add a new node for the neutron-gateway
    - `machine 4`
  - add tags
    - machines 0..3: `tags=converged`
    - machine 4: `tags=neutron-gateway`
  - correct the "data-port" interface name
    - neutron-gateway:
      - data-port: `br-ex:eth1`
      - to: `4`
  - change the virt-type fron kvm to qemu
    - nova-compute:
      - `virt-type: qemu`

```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ cp bundle.yaml bundle.yaml--
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ diff bundle.yaml bundle.yaml--
4d3
<     constraints: tags=converged
7d5
<     constraints: tags=converged
10d7
<     constraints: tags=converged
13,16d9
<     constraints: tags=converged
<   '4':
<     series: bionic
<     constraints: tags=neutron-gateway
215c208
<       data-port: br-ex:eth1
---
>       data-port: br-ex:eno2
219c212
<     - '4'
---
>     - '0'
250d242
<       virt-type: qemu
```

- deploy

```
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ MODEL=`basename \`pwd\``
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ juju add-model $MODEL olab
ubuntu@r0:~/lab/openstack/openstack-base-bionic-train$ juju deploy ./bundle.yaml
```

