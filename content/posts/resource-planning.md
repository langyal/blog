+++
title = "Resource Planning"
date = 2018-09-01T21:56:08+02:00
draft = false
tags = []
categories = []
+++

`GOAL` of this post: **resource planning**

This planning went thru some iterations... I started with 4GB nodes and you will read everywhere that you need 2GB RAM for juju.
Despite the fact that I like even numbers in memory definition and odd numbers in clustering, I had to accept that I don't have more RAM... believe me, this will work:

### amount of memory in our server: 16GB RAM

| unit        | memsize | note |
| ----------- |:-------:| ---- |
| hypervisor  | n/a     | this is the host OS - will be allocated implicitly |
| maas        | 2GB     | maas: KVM |
| juju        | 1GB     | maas: KVM |
| neutron     | 1GB     | neutron-gateway |
| node-a      | 3GB     | converged design: control, compute, ceph, swift |
| node-b      | 3GB     | converged design: control, compute, ceph, swift |
| node-c      | 3GB     | converged design: control, compute, ceph, swift |

### node design: maas
| memsize | cpu | disk-1 | disk-2 | disk-3 | iface-1 | iface-2 | iface-3 |
| :-----: | :-: | :----: | :----: | :----: | :-----: | :-----: | :-----: |
| 2GB     | 1   | 20GB   | ---    | ---    | default | net-ext | net-int |

### node design: juju
| memsize | cpu | disk-1 | disk-2 | disk-3 | iface-1 | iface-2 | iface-3 |
| :-----: | :-: | :----: | :----: | :----: | :-----: | :-----: | :-----: |
| 1GB     | 1   | 8GB    | ---    | ---    | net-ext | ---     | ---     |

### node design: neutron
| memsize | cpu | disk-1 | disk-2 | disk-3 | iface-1 | iface-2 | iface-3 |
| :-----: | :-: | :----: | :----: | :----: | :-----: | :-----: | :-----: |
| 1GB     | 1   | 30GB   | ---    | ---    | net-ext | net-int | ---     |

### node design: node-{a|b|c}
| memsize | cpu | disk-1 | disk-2 | disk-3 | iface-1 | iface-2 | iface-3 |
| :-----: | :-: | :----: | :----: | :----: | :-----: | :-----: | :-----: |
| 3GB     | 1   | 30GB   | 8GB    | 8GB    | net-ext | net-int | ---     |

Finally, let's see how our setup should look like from the network point of view. The dashed line means that a network connection is there but we don't really
want to use it; typically, libvirt creates an interface on the hypervisor by default every time you create a network; we keep it because this way we can reach the
units on those networks directly - the hypervisor acts as a jumphost.

![network](/4lab-node-network.png)

