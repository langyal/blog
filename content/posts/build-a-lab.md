+++
title = "Build your own Lab"
date = 2018-08-27T20:43:20+02:00
draft = false
tags = []
categories = []
+++

`GOAL` of this post: **build your own LAB**

### prerequisites
- take a machine with 16GB RAM at least; you need a fairly good internet connection also
- we will prepare this node as a *hypervisor*
- we need nested virtualization (just like in the film *Inception*)
  - I hope your processor supports it
  - go to the BIOS and check/enable it
- we will use the following technologies:
  - Ubuntu Desktop 18.04 LTS - because I have a notebook and eventually I want to use it as a desktop
  - KVM/qemu - not virtualbox, because KVM is a more server-like technology

Even if it's a Desktop version, we will not accept the default installation. Why? Because we are *experts* :) And because we want to

- use LVM
- reserve some disk space in the volume group so that we can use snapshots

This is what you have to do (your disk device name may vary):

### boot from external media (usb), open a terminal

```
[mediaboot]# fdisk /dev/nvme0n1
[mediaboot]# fdisk -l /dev/nvme0n1
Disk /dev/nvme0n1: 477 GiB, 512110190592 bytes, 1000215216 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xb04e826c

Device         Boot   Start        End   Sectors   Size Id Type
/dev/nvme0n1p1 *       2048    1050623   1048576   512M ef EFI (FAT-12/16/32)
/dev/nvme0n1p2      1050624 1000215215 999164592 476.4G 83 Linux

[mediaboot]# pvcreate /dev/nvme0n1p2
[mediaboot]# vgcreate vg-root /dev/nvme0n1p2
[mediaboot]# lvcreate -L100G -n lv-root vg-root
[mediaboot]# lvcreate -L1G -n lv-swap vg-root
[mediaboot]# mkfs.ext4 /dev/mapper/vg--root-lv--root
[mediaboot]# mkswap -f /dev/mapper/vg--root-lv--swap
```

### start the installer
- Installation Type: choose "Something else"
  - select /dev/nvme0n1p1 "use as": EFI System Partition
  - select /dev/mapper/vg-root-lv-root "use as": Ext4 journaling file system, "Mount point": /
  - select /dev/mapper/vg-root-lv-swap "use as": swap area
  - ignore the rest of the screen, confusing
- Install Now


### install sshd
```
u@hyp:~$ sudo apt install -y openssh-server
```

### install cpu checker
```
u@hyp:~$ sudo apt install -y cpu-checker
u@hyp:~$ sudo kvm-ok
INFO: /dev/kvm exists
KVM acceleration can be used
```

### install some networking tools

```
u@hyp:~$ sudo apt install -y net-tools
```

### install virtualization and related goodies

```
u@hyp:~$ sudo apt install -y qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virt-manager
```

### prepare libvirt networking

- add *net-ext* and *net-int* networks
- we will use those networks later

```
u@hyp:~$ cat net-ext.xml
<network>
  <name>net-ext</name>
  <bridge name="br-ext"/>
  <ip address="192.168.100.1" netmask="255.255.255.0"/>
</network>

u@hyp:~$ cat net-int.xml
<network>
  <name>net-int</name>
  <bridge name="br-int"/>
  <ip address="192.168.200.1" netmask="255.255.255.0"/>
</network>

u@hyp:~$ virsh net-define net-ext.xml
u@hyp:~$ virsh net-start net-ext
u@hyp:~$ virsh net-autostart net-ext

u@hyp:~$ virsh net-define net-int.xml
u@hyp:~$ virsh net-start net-int
u@hyp:~$ virsh net-autostart net-int

u@hyp:~$ virsh net-list
 Name                 State      Autostart     Persistent
----------------------------------------------------------
 default              active     yes           yes
 net-int              active     yes           yes
 net-ext              active     yes           yes
```

### add user `ubuntu` to power on/off KVM nodes

```
u@hyp:~$ sudo useradd -G libvirt ubuntu
u@hyp:~$ sudo passwd ubuntu
```

## warning: the following steps are not necessary, but we might want to use this hypervisor as an `lxd` host also later

### install lxd via snap
- the client is also bundled, now it's called lxd.lxc
- we will create a new lxc profile to be able to launch an instance on the `br-ext` bridge
- on `br-ext` our (future) maas server will provide DHCP

```
u@hyp:~$ sudo snap install lxd
u@hyp:~$ sudo lxd init      # accept the defaults
u@hyp:~$ sudo adduser u lxd # logout/login
u@hyp:~$ id
uid=1000(u) gid=1000(u) groups=1000(u),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),116(lpadmin),126(sambashare),129(libvirt),999(lxd)
u@hyp:~$ alias lxc=lxd.lxc

u@hyp:~$ lxc profile list
+---------+---------+
|  NAME   | USED BY |
+---------+---------+
| default | 0       |
+---------+---------+

u@hyp:~$ lxc profile show default
...
devices:
  eth0:
    name: eth0
    nictype: bridged
    parent: lxdbr0
    type: nic
...

u@hyp:~$ lxc profile copy default net-ext
u@hyp:~$ lxc profile device set net-ext eth0 parent br-ext
...
devices:
  eth0:
    name: eth0
    nictype: bridged
    parent: br-ext
    type: nic
...

u@hyp:~$ lxc launch ubuntu:xenial x1
u@hyp:~$ lxc launch -p net-ext ubuntu:xenial x2 # note: if maas is not configured, this instance will have no ip address now

```

### install vagrant + libvirt plugin -> we can deploy a kvm/qemu VM with vagrant later

```
u@hyp:~$ sudo apt install -y vagrant
u@hyp:~$ vagrant plugin install vagrant-libvirt
```

### install uvtool, get an image -> we can deploy a kvm/qemu VM from a cloud image later
```
u@hyp:~$ sudo apt -y install uvtool
u@hyp:~$ uvt-simplestreams-libvirt sync release=xenial arch=amd64
u@hyp:~$ uvt-simplestreams-libvirt query
release=xenial arch=amd64 label=release (20180806)
```

At the end, the hypervisor's network should look like this:

![overview](/0001-3lab-hypervisor-network.png)

### That's it for now; we have:
- a hypervisor based on Ubuntu Desktop 18.04 (bionic)
- kvm enabled
- lxd enabled

### references
- https://wiki.libvirt.org/page/VirtualNetworking
- https://libvirt.org/formatnetwork.html#examplesPrivate
- https://blog.simos.info/how-to-make-your-lxd-containers-get-ip-addresses-from-your-lan-using-a-bridge/
- https://github.com/vagrant-libvirt/vagrant-libvirt#libvirt-configuration
- https://help.ubuntu.com/lts/serverguide/cloud-images-and-uvtool.html
- https://www.cyberciti.biz/faq/how-to-use-kvm-cloud-images-on-ubuntu-linux/


