
## overcloud prepare

### prepare `r0` as a gw for the overlay
- you find overlay-config.sh [here](/r0.tgz)

```
ubuntu@r0:~$ sudo ./overlay-config.sh 10 start
ubuntu@r0:~$ sudo ip addr add 10.33.0.1/16 dev br-vxlan10
ubuntu@r0:~$ sudo ip addr add 10.44.0.1/16 dev br-vxlan10
```

### set r0 snat for `10.33/16` and `10.44/16`
```
ubuntu@r0:~$ sudo bash -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
ubuntu@r0:~$ echo 'net.ipv4.ip_forward=1' | sudo tee -a /etc/sysctl.conf
ubuntu@r0:~$ sudo iptables -t nat -A POSTROUTING -s 10.33.0.0/16 -o ens3 -j MASQUERADE
ubuntu@r0:~$ sudo iptables -t nat -A POSTROUTING -s 10.44.0.0/16 -o ens3 -j MASQUERADE
ubuntu@r0:~$ sudo apt install -y iptables-persistent # save current rules
```

## prepare vm-in-vm templates
- you find what you need [here](/r0.tgz)

```
ubuntu@r0:~/lab/domain-xml$ ./gen
ubuntu@r0:~/lab/domain-xml$ ./dist
```

## oc-11maas: a VM inside uc-node11 with static IP
- we don't have dhcp/cloud-init provider
- we use the cloud-init configdrive to provide ip/public-key

### download the cloud image first
```
ubuntu@r0:~$ ssh uc-node11
ubuntu@uc-node11:~$ export http_proxy=10.22.0.3:8888
ubuntu@uc-node11:~$ export https_proxy=10.22.0.3:8888
ubuntu@uc-node11:~$ wget https://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img
```

### resize the image
```
ubuntu@uc-node11:~$ qemu-img info xenial-server-cloudimg-amd64-disk1.img
image: xenial-server-cloudimg-amd64-disk1.img
file format: qcow2
virtual size: 2.2G (2361393152 bytes)
disk size: 283M
cluster_size: 65536
Format specific information:
    compat: 0.10
    refcount bits: 16
ubuntu@uc-node11:~$ qemu-img resize xenial-server-cloudimg-amd64-disk1.img 20G
ubuntu@uc-node11:~$ qemu-img info xenial-server-cloudimg-amd64-disk1.img | grep "virtual size:"
virtual size: 20G (21474836480 bytes)
```

### create a COW copy from the base image:
```
ubuntu@uc-node11:~$ qemu-img create -f qcow2 -b xenial-server-cloudimg-amd64-disk1.img oc-11maas.img
Formatting 'oc-11maas.img', fmt=qcow2 size=4294967296 backing_file=xenial-server-cloudimg-amd64-disk1.img encryption=off cluster_size=65536 lazy_refcounts=off refcount_bits=16
```

### prepare configdrive data
- you find what you need [here](/r0.tgz)

```
ubuntu@uc-node11:~$ cat oc-11maas-userdata.xml
#cloud-config

password: qwe321
chpasswd:
  expire: false
ssh_pwauth: no
ssh_authorized_keys:
  - ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAIEAxGMFh/IYklxg1i+LnHR1yFd8Mrk8d4bDjdeLv99pdi0ucER04ul+Rd5bAGyCc3FITbu5M7EHOQLJysk2ddSsjfqfyem3DZ0VmpeT+ta1G33gjoV6+7xj9ZN18fneD/cXyNYQwh0VdjBfIg0aoUuTdgMznVMsBUfdsrU6FFnc3X8= langyal@icon.hu

ubuntu@uc-node11:~$ cat oc-11maas-netconfig.xml
version: 1
config:
  - type: nameserver
    address:
      - 10.22.0.3
  - type: physical
    name: ens2
    subnets:
     - control: auto
       type: static
       address: 10.33.0.11/16
       gateway: 10.33.0.1
```

### create the configdrive
```
ubuntu@uc-node11:~$ sudo apt install cloud-image-utils -y
ubuntu@uc-node11:~$ cloud-localds -N oc-11maas-netconfig.xml oc-11maas-userdata.img oc-11maas-userdata.xml
```

### move the images to the proper target directory
```
ubuntu@uc-node11:~$ sudo mv xenial-server-cloudimg-amd64-disk1.img oc-11maas-userdata.img oc-11maas.img /img
```

### define the vm-in-vm
```
ubuntu@uc-node11:~$ virsh define oc-11maas.xml
ubuntu@uc-node11:~$ virsh list --all
 Id    Name                           State
----------------------------------------------------
 -     oc-11maas                      shut off
```

### launch the vm-in-vm
```
ubuntu@uc-node11:~$ virsh start oc-11maas
ubuntu@uc-node11:~$ virsh console oc-11maas
```

### ssh to the vm-in-vm
```
ubuntu@r0:~$ ssh oc-11maas
```

## maas

### install maas
```
ubuntu@oc-11maas:~$ sudo bash -c "echo 'Acquire::http::Proxy \"http://10.22.0.3:8888\";' >/etc/apt/apt.conf.d/02proxy"
ubuntu@oc-11maas:~$ sudo apt update && sudo apt upgrade -y
ubuntu@oc-11maas:~$ sudo apt install python maas libvirt-bin -y
ubuntu@oc-11maas:~$ newgrp libvirtd
ubuntu@oc-11maas:~$ virsh net-destroy default
ubuntu@oc-11maas:~$ virsh net-undefine default
```

### provide access from maas to the "hypervisors" (uc-node11..18)
```
- create keypair
ubuntu@oc-11maas:~$ sudo chsh -s /bin/bash maas
ubuntu@oc-11maas:~$ sudo su - maas
maas@oc-11maas:~$ ssh-keygen -f ~/.ssh/id_rsa -N ''

- fetch the public key
ubuntu@r0:~/lab/ansible$ ansible all -b --become-method sudo --become-user root --limit oc-11maas -m fetch -a "src=/var/lib/maas/.ssh/id_rsa.pub dest=."
ubuntu@r0:~/lab/ansible$ mkdir -p plays/public_keys
ubuntu@r0:~/lab/ansible$ mv oc-11maas/var/lib/maas/.ssh/id_rsa.pub plays/public_keys/maas

- distribute the public key
ubuntu@r0:~/lab/ansible$ ansible-playbook plays/uc-hypervisor.yml

- copy /etc/hosts from r0 to oc-11maas
ubuntu@r0:~/lab/ansible$ ansible all -b --become-method sudo --become-user root --limit oc-11maas -m copy -a "src=/etc/hosts dest=/etc/hosts"

- test it: ssh from maas vm-in-vm to uc-node12 vm
ubuntu@oc-11maas:~$ sudo su - maas
maas@oc-11maas:~$ ssh ubuntu@uc-node11
maas@oc-11maas:~$ ssh ubuntu@uc-node12
maas@oc-11maas:~$ ssh ubuntu@uc-node13
maas@oc-11maas:~$ ssh ubuntu@uc-node14
maas@oc-11maas:~$ ssh ubuntu@uc-node15
maas@oc-11maas:~$ ssh ubuntu@uc-node16
maas@oc-11maas:~$ ssh ubuntu@uc-node17
maas@oc-11maas:~$ ssh ubuntu@uc-node18
```

### configure maas and access the maas gui
- create admin user
```
ubuntu@oc-11maas:~$ sudo maas createadmin
Username: maasadmin
Password:
Again:
Email: laszlo.angyal@pan-net.eu
Import SSH keys [] (lp:user-id or gh:user-id):
```

- from your local machine
```
ssh -L8000:oc-11maas:80 ubuntu@$FIP
```

- from a browser:
```
http://localhost:8000/MAAS/
```

### configure maas in the gui
- maas region: **olab**

- DNS forwarder: **10.22.0.3**

- APT & HTTP/HTTPS proxy server: **10.22.0.3:8888**

- maas images:

```
Ubuntu / Choose source / maas.io
  - 18.04 LTS
  - 16.04 LTS
```

- upload your ssh public key

- settings / Global Kernel Parameters: **net.ifnames=0**

- Proxy
  - External: **http://10.22.0.3:8888**

- DNS: **10.22.0.3**

- NTP servers: **10.22.0.3**

- Network Discovery: **Disabled**

- Storage / Use secure erase by default when erasing disks: **uncheck**

- cleanup
  - remove subnet: **192.168.122.0/24**
  - remove fabric: **fabric-1**

- enable dhcp
  - go to Subnets, choose **fabric-0**
  - click on "VLAN" "untagged" and choose action "Provide DHCP"
  - click on "Subnet" and set the nameserver to 10.22.0.3

- reserve ranges:

```
10.33.0.1 - 10.33.0.255 # oc-node addresses
10.33.1.1 - 10.33.1.255 # openstack VIP addresses
```

## feed maas

### create "empty" instances and enlist them in maas

- create volumes
- define and start vm-in-vm instances
- wait until you see 10 `New` nodes in maas

```
ubuntu@r0:~$ for i in 14 15 16 17 18; do
UC=uc-node$i
 ssh $UC virsh vol-create-as img oc-${i}01-a.img 20G --format qcow2
 ssh $UC virsh vol-create-as img oc-${i}01-b.img 20G --format qcow2
 ssh $UC virsh define oc-${i}01.xml
 ssh $UC virsh start oc-${i}01
 ssh $UC virsh vol-create-as img oc-${i}02-a.img 20G --format qcow2
 ssh $UC virsh vol-create-as img oc-${i}02-b.img 20G --format qcow2
 ssh $UC virsh define oc-${i}02.xml
 ssh $UC virsh start oc-${i}02
done
```

## maas power-parameters

### get the maas private key from the maas vm-in-vm
- homework: copy `oc-11maas:~maas/.ssh/id_rsa` to `r0:~ubuntu/.ssh/id_rsa`

```
ubuntu@r0:~$ ls -l ~/.ssh/id_rsa
-r-------- 1 ubuntu ubuntu 1675 Dec 10 11:17 /home/ubuntu/.ssh/id_rsa
```

### gather oc- instances
- restriction: instance names must begin with 'oc-'

```
ubuntu@r0:~$ mkdir ~/lab/maas-inventory && cd ~/lab/maas-inventory
ubuntu@r0:~/lab/maas-inventory$ for X in 14 15 16 17 18; do UC=uc-node$X; virsh --connect qemu+ssh://$UC/system list --all | grep oc- | awk '{print $2}' >$UC; done
```

### gather mac addresses
```
ubuntu@r0:~/lab/maas-inventory$ for X in 14 15 16 17 18; do UC=uc-node$X; for OC in `cat $UC`; do virsh --connect qemu+ssh://$UC/system dumpxml $OC | grep "mac address" | awk -F\' '{print $2}' >$UC-$OC; done; done
```

### set maas parameters based on the mac addresses

```
- get the API key as root on the maas vm-in-vm
root@oc-11maas:~# maas apikey --username maasadmin
fFKnRBvhxFYqp3gKYR:bjBKSAqV7QnRVctY7J:W7GFCzdqx3LegBQxeZdx77rDKp3SFabc

- login to maas
ubuntu@r0:~/lab/maas-inventory$ maas login maasadmin http://oc-11maas/MAAS/
API key (leave empty for anonymous access):

You are now logged in to the MAAS server at
http://oc-11maas/MAAS/api/2.0/ with the profile name 'maasadmin'.

- create tags - we will use them later
ubuntu@r0:~$ TAG=converged
ubuntu@r0:~$ maas maasadmin tags create name=$TAG
ubuntu@r0:~$ TAG=juju
ubuntu@r0:~$ maas maasadmin tags create name=$TAG

- set the parameters and tag them - we will need it later
ubuntu@r0:~/lab/maas-inventory$ for X in 14 15 16 17 18; do UC=uc-node$X; for OC in `cat $UC`; do 
TAG=converged
MAC=`head -1 $UC-$OC`
SYSID=`maas maasadmin nodes read mac_address=$MAC | jq '.[] | .system_id' | sed 's/"//g'`
POWERADDRESS="qemu+ssh://ubuntu@$UC/system"
maas maasadmin machine update $SYSID hostname=$OC power_type=virsh power_parameters_power_address="$POWERADDRESS" power_parameters_power_id=$OC >/dev/null
maas maasadmin tag update-nodes $TAG add=$SYSID
done
done
```

### commission the nodes
```
ubuntu@r0:~/lab/maas-inventory$ maas maasadmin machines accept-all
```

