+++
title = "Purpose of My Blog"
date = 2018-08-27T15:43:19+02:00
draft = false
tags = []
categories = []
+++

Well, the primary goal is obvious: share the knowledge I got from others.
The second reason is that if you decide to publish something you must really understand what you are talking about - so your own knowledge will be implicitly deeper.
Beside self-education, I always liked to explain something in a consumable way; the style of a blog can be lazy, this is not an RFC or API specification.
Let's see how it succeeds...

One might say that "hey, you can find all of this on the net, why are you doing this"? This is just partially true: first, what you publish *today* will be probably invalid in a few weeks/months; second, what I post here is *unique* because it is configured and tested by myself and this way I can guarantee that you can repeat it step-by-step on your own.

I plan to publish a series of posts forming an *epic* or *story* (yes, I attended some training on agility...). I know people will not read a post if it takes more than 10 minutes (or just 5?). I can promise that a story will have a *target* and each post will have a *goal*.

If you have a story, you can tell it in one of two styles (or more):

- Columbo-style: start with the *outcome* and build the story around the *way* you get there
- Poirot-style: start somehow and keep the outcome for yourself - and let the reader figure out where you want to get...

I like both gentlemen for different reasons. I will try to mix these styles.

Intended audience: anyone who is interested in

- unix/linux
- cloudification
- automation
- soccer (this is not strictly required)

