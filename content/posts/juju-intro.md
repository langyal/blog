---
title: "Juju Intro"
date: 2018-10-30T16:59:51+01:00
draft: false
---

`GOAL` of this post: **get familiar with juju**

Before we do something really complicated, let's start with some simple exercises.

We create a new juju model called _dummy_ and deploy our first charm/application: _ubuntu_.

```
u@hyp:~$ juju add-model dummy
Uploading credential 'lab.local/admin/maasadmin' to controller
Added 'dummy' model with credential 'maasadmin' for user 'admin'

u@hyp:~$ juju models
Controller: jc-maas

Model       Cloud/Region  Status     Machines  Cores  Access  Last connection
controller  lab.local     available         1      1  admin   just now
default     lab.local     available         0      -  admin   2018-08-10
dummy*      lab.local     available         0      -  admin   never connected

u@hyp:~$ juju status
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  16:56:36+01:00

Model "admin/dummy" is empty.

u@hyp:~$ juju deploy ubuntu
Located charm "cs:ubuntu-12".
Deploying charm "cs:ubuntu-12".
```

You can see that juju works in async mode; it returned - but the job hasn't been finished at all:

```
u@hyp:~$ juju status
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  17:06:32+01:00

App     Version  Status   Scale  Charm   Store       Rev  OS      Notes
ubuntu           waiting    0/1  ubuntu  jujucharms   12  ubuntu

Unit      Workload  Agent       Machine  Public address  Ports  Message
ubuntu/0  waiting   allocating  0        192.168.100.44         waiting for machine

Machine  State    DNS             Inst id  Series  AZ       Message
0        pending  192.168.100.44  d3rqq4   xenial  default  Deploying: 'curtin' curtin command install
```

Meanwhile, checking the console of the VM you can see that

- maas powered the VM on
- the VM started to boot over the network
- maas deployed ubuntu from the cloud image and installed the juju agent via cloud-init
- maas rebooted the VM which booted via the network, but this time maas redirected it to boot from local disk
- juju agent connected to the controller and populated an appropriate ssh public key

Check the status again:
```
u@hyp:~$ juju status
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  17:37:38+01:00

App     Version  Status  Scale  Charm   Store       Rev  OS      Notes
ubuntu  16.04    active      1  ubuntu  jujucharms   12  ubuntu

Unit       Workload  Agent  Machine  Public address  Ports  Message
ubuntu/0*  active    idle   0        192.168.100.44         ready

Machine  State    DNS             Inst id  Series  AZ       Message
0        started  192.168.100.44  d3rqq4   xenial  default  Deployed
```

You can login to the unit this way:

```
u@hyp:~$ juju ssh ubuntu/0
Welcome to Ubuntu 16.04.5 LTS (GNU/Linux 4.4.0-138-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  Get cloud support with Ubuntu Advantage Cloud Guest:
    http://www.ubuntu.com/business/services/cloud

0 packages can be updated.
0 updates are security updates.

New release '18.04.1 LTS' available.
Run 'do-release-upgrade' to upgrade to it.


Last login: Tue Oct 30 16:26:23 2018 from 192.168.100.1
ubuntu@aaaa:~$ 
```

Now let's try to add a _unit_ to our _application_ called _ubuntu_:
```
u@hyp:~$ juju add-unit ubuntu
u@hyp:~$ juju status 
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  17:42:55+01:00

App     Version  Status   Scale  Charm   Store       Rev  OS      Notes
ubuntu           waiting    1/2  ubuntu  jujucharms   12  ubuntu

Unit       Workload  Agent       Machine  Public address  Ports  Message
ubuntu/0*  active    idle        0        192.168.100.44         ready
ubuntu/1   waiting   allocating  1                               waiting for machine

Machine  State    DNS             Inst id  Series  AZ       Message
0        started  192.168.100.44  d3rqq4   xenial  default  Deployed
1        pending                  pending  xenial           failed to start machine 1 (failed to acquire node: No available machine matches constraints: [('agent_name', ['401e5b3a-1b4b-4591-8143-4b9de0a20846']), ('zone', ['default'])] (resolved to "zone=default")), retrying in 10s (10 more attempts)
```

This time it fails since there is "No available machine" in maas now. Let's create one more - you should be able to do that if you read my previous posts.

Let's try it again:

```
u@hyp:~$ juju remove-unit ubuntu/1
removing unit ubuntu/1

u@hyp:~$ juju add-unit ubuntu

u@hyp:~$ juju status 
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  17:56:02+01:00

App     Version  Status   Scale  Charm   Store       Rev  OS      Notes
ubuntu           waiting    1/2  ubuntu  jujucharms   12  ubuntu

Unit       Workload  Agent       Machine  Public address  Ports  Message
ubuntu/0*  active    idle        0        192.168.100.44         ready
ubuntu/2   waiting   allocating  2                               waiting for machine

Machine  State    DNS             Inst id  Series  AZ       Message
0        started  192.168.100.44  d3rqq4   xenial  default  Deployed
2        pending                  r7x6c4   xenial  default  starting

u@hyp:~$ juju status | tail -2
2        pending  192.168.100.47  r7x6c4   xenial  default  Deploying: ubuntu/amd64/ga-16.04/xenial/daily/boot-initrd

u@hyp:~$ juju status | tail -2
2        pending  192.168.100.47  r7x6c4   xenial  default  Deploying: 'cloudinit' searching for network datasources

u@hyp:~$ juju status | tail -2
2        pending  192.168.100.47  r7x6c4   xenial  default  Deploying: 'curtin' curtin command install

u@hyp:~$ juju status | tail -2
2        pending  192.168.100.47  r7x6c4   xenial  default  Deploying: 'curtin' installing kernel

u@hyp:~$ juju status | tail -2
2        pending  192.168.100.47  r7x6c4   xenial  default  Deploying: 'cloudinit' running modules for final

u@hyp:~$ juju status | tail -2
2        started  192.168.100.47  r7x6c4   xenial  default  Deployed

```

Now try to login:

```
u@hyp:~$ juju ssh ubuntu/1
ERROR unit "ubuntu/1" not found
```

Oops, where is unit/1 ? Well, it's gone for ever: juju increases this number by one always. So

- the first unit is 0
- the second unit is 1
- now remove the second unit
- the next unit will be 2, despite the fact that the unit number 1 is now "free"
- of course this works now: `juju ssh ubuntu/2`

This way you can see the "dynamics" of a juju-managed environment... higher unit numbers mean more fluctuation.

Let's try something else now; first, clean up our model:
```
u@hyp:~$ juju remove-unit ubuntu/2
removing unit ubuntu/2

u@hyp:~$ juju remove-unit ubuntu/0
removing unit ubuntu/0

u@hyp:~$ juju remove-application ubuntu
removing application ubuntu

u@hyp:~$ juju status
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  18:21:41+01:00

Model "admin/dummy" is empty.
```

This time we will set up the "hello world" of juju:
```
u@hyp:~$ juju deploy mysql
u@hyp:~$ juju deploy wordpress

u@hyp:~$ juju status
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  18:52:44+01:00

App        Version  Status   Scale  Charm      Store       Rev  OS      Notes
mysql      5.7.24   active       1  mysql      jujucharms   58  ubuntu
wordpress           waiting    0/1  wordpress  jujucharms    5  ubuntu

Unit         Workload  Agent       Machine  Public address  Ports     Message
mysql/0*     active    idle        3        192.168.100.40  3306/tcp  Ready
wordpress/0  waiting   allocating  4                                  waiting for machine

Machine  State    DNS             Inst id  Series  AZ       Message
3        started  192.168.100.40  d3rqq4   xenial  default  Deployed
4        down                     pending  trusty           unexpected: ServerError: 400 BAD REQUEST ({"distro_series": ["'trusty' is not a valid distro_series.  It should be one of: '', 'ubuntu/xenial', 'ubuntu/bionic'."]})
```

Well, we have to add the _trusty_ image to maas also... (you should be able to do it yourself :). Let's continue:
```
u@hyp:~$ juju remove-unit wordpress/0
removing unit wordpress/0

u@hyp:~$ juju remove-application wordpress
removing application wordpress

u@hyp:~$ juju deploy wordpress
```

...and the real magic:
```
u@hyp:~$ juju add-relation wordpress mysql
u@hyp:~$ juju status
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  19:01:11+01:00

App        Version  Status       Scale  Charm      Store       Rev  OS      Notes
mysql      5.7.24   active           1  mysql      jujucharms   58  ubuntu
wordpress           maintenance      1  wordpress  jujucharms    5  ubuntu

Unit          Workload     Agent      Machine  Public address  Ports     Message
mysql/0*      active       idle       3        192.168.100.40  3306/tcp  Ready
wordpress/1*  maintenance  executing  5        192.168.100.45  80/tcp    performing configuration changes

Machine  State    DNS             Inst id  Series  AZ       Message
3        started  192.168.100.40  d3rqq4   xenial  default  Deployed
5        started  192.168.100.45  r7x6c4   trusty  default  Deployed

```

Yes, this is true: the "mysql" and the "wordpress" charms could "talk to each other" and the frontend/backend configuration happened automagically.
```
u@hyp:~$ juju status
Model  Controller  Cloud/Region  Version  SLA          Timestamp
dummy  jc-maas     lab.local     2.4.1    unsupported  19:09:55+01:00

App        Version  Status  Scale  Charm      Store       Rev  OS      Notes
mysql      5.7.24   active      1  mysql      jujucharms   58  ubuntu
wordpress           active      1  wordpress  jujucharms    5  ubuntu

Unit          Workload  Agent  Machine  Public address  Ports     Message
mysql/0*      active    idle   3        192.168.100.40  3306/tcp  Ready
wordpress/1*  active    idle   5        192.168.100.45  80/tcp

Machine  State    DNS             Inst id  Series  AZ       Message
3        started  192.168.100.40  d3rqq4   xenial  default  Deployed
5        started  192.168.100.45  r7x6c4   trusty  default  Deployed
```

Quick cleanup:
```
u@hyp:~$ juju destroy-model dummy
WARNING! This command will destroy the "dummy" model.
This includes all machines, applications, data and other resources.

Continue [y/N]? y
Destroying model
Waiting on model to be removed, 2 machine(s), 2 application(s)...
Waiting on model to be removed, 2 machine(s), 2 application(s)...
Waiting on model to be removed, 1 machine(s)...
Waiting on model to be removed...
Model destroyed.
```

### summary
Now you know what juju can do for you: deploy your model. The next blog post will talk about a full openstack deployment with juju.

