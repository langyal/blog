
## references

### vxlan
- https://ilearnedhowto.wordpress.com/2017/02/16/how-to-create-overlay-networks-using-linux-bridges-and-vxlans/
- https://github.com/Mellanox/mlxsw/wiki/Virtual-eXtensible-Local-Area-Network-(VXLAN)

### cloud images with configdrive
- https://www.theurbanpenguin.com/using-cloud-images-in-kvm/
- https://gist.github.com/cjihrig/a0f0e3c058b4d9dcf9ca1f771916fa28

### openstack in VMs
- http://chrisarges.net/2014/06/13/manually-deploying-openstack-with.html
- https://langyal.gitlab.io/blog/posts/story-build-openstack/

### maas ansible
- https://everythingshouldbevirtual.com/automation/ansible-maas-management/

### bind on bionic
- https://www.linuxbabe.com/ubuntu/set-up-local-dns-resolver-ubuntu-18-04-16-04-bind9

### djbdns
- https://en.wikipedia.org/wiki/Dbndns
- http://www.nemostar.org/djbdns/

